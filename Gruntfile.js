/*!
 * Rimanews Grunt Config
 * Authored by Arvianto Dwi, 2015-2016
 */
module.exports = function(grunt) {
  // Load all packages in development scope with "grunt-" file name pattern.
  // See package.json for packages list and detail.
  require('load-grunt-tasks')(grunt, {scope: 'devDependencies'});

  // Load some postcss plugin and assign each of it to variable
  var autoprefixer = require('autoprefixer');
  var cssnano = require('cssnano');

  // Grunt project configuration
  grunt.initConfig({
    pkg: grunt.file.readJSON('package.json'),

    // Config: grunt-sass
    sass: {
      compile: {
        options: { outputStyle: 'expanded' },
        files: [{
          src: './source/sass/rimanews.scss',
          dest: './public/css/rimanews.css'
        }]
      }
    },

    // Config: grunt-postcss
    postcss: {
      devBuild: {
        options: {
          processors: [
            autoprefixer({
              browsers: ['> 1%', 'last 2 versions', 'ie 8', 'ie 9', 'Opera 12.1']
            })
          ]
        },
        files: [{
          src: './public/css/rimanews.css',
          dest: './public/css/rimanews.css'
        }]
      },
      distBuild: {
        options: {
          processors: [ cssnano() ],
          map: {
            inline: false,
            annotation: './public/css/'
          }
        },
        files: [{
          expand: true,
          cwd: './public/css/',
          src: 'rimanews.css',
          dest: './public/css/',
          ext: '.min.css',
          extDot: 'last',
          // rename: function(dest, src) {
          //   // return dest + src.replace('temp.', '');
          // }
        }]
      }
    },

    // Config: grunt-contrib-jshint
    jshint: {
      all: [ './source/js/**/*.js' ]
    },

    // Config: grunt-contrib-uglify
    uglify: {
      options: {
        sourceMap: true
      },
      minBuild: {
        files: [{
          expand: true,
          cwd: './source/js/',
          src: '*.js',
          dest: './public/js/',
          ext: '.min.js',
          extDot: 'last'
        }]
      }
    },

    // Config: grunt-contrib-clean
    clean: {
      tempCss: [ './public/css/temp.*.css' ]
    },

    // Config: grunt-contrib-watch
    watch: {
      scss: {
        files: [ './source/sass/*.scss', './source/sass/**/*.scss' ],
        tasks: [ 'sass:compile', 'postcss:devBuild', 'postcss:distBuild', 'clean:tempCss' ]
      },
      js: {
        files: [ './source/js/**/*.js' ],
        tasks: [ 'jshint:all', 'uglify:minBuild' ]
      }
    }
  });

  // Set SASS compiling before doing a file watching
  grunt.config('compile-first', grunt.option('compile-first') || undefined);
  var sassTasks = ( typeof grunt.config('compile-first') === 'undefined' ) ?
                   [ 'watch:scss' ] :
                   [ 'sass:compile', 'postcss:devBuild', 'clean:tempCss', 'watch:scss' ];

  // Set grunt task
  grunt.registerTask('watch-scss', sassTasks);
  grunt.registerTask('watch-js', 'watch:js');
};
