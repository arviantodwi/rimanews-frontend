/*!
 * Rimanews Webpack Config
 * Authored by Arvianto Dwi, 2015-2016
 */
var webpack = require('webpack');

module.exports = {
  entry: './source/jsx/App.js',
  output: {
    path: __dirname,
    filename: './public/js/app.js'
  },
  module: {
    loaders: [{
      test: /\.jsx?$/,
      exclude: /node_modules/,
      loader: 'babel',
      query: {
        presets: ['es2015', 'react']
      }
    }]
  }
};