/* jshint esversion: 6 */
/* jshint loopfunc: true */
/*!
 * RimanewsJS v1.0.0
 * 
 * Copyright 2016, PT. Rimanews Indonesia
 * http://www.rimanews.com
 * Released on: -
 */
(function() {
  'use strict';

  /////////////
  // Helpers //
  /////////////
  /**
   * [isArray description]
   * @param  {[type]}  obj [description]
   * @return {Boolean}     [description]
   */
  function isArray(obj) {
    if( !Array.isArray ) {
      var stringOfType = Object.prototype.toString.apply(obj);
      return (stringOfType === '[object Array]') ? true : false;
    }
    else return Array.isArray(obj);
  }
  /**
   * [arrayContains description]
   * @param  {[type]} haystack      [description]
   * @param  {[type]} needle        [description]
   * @param  {[type]} iterationType [description]
   * @return {[type]}               [description]
   */
  function arrayIsContains(haystack, needle, iterationType) {
    var i = (iterationType === 'ascending') ? 0 : haystack.length - 1,
        max_i = (iterationType === 'ascending') ? haystack.length : -1;

    if( iterationType === 'ascending' ) {
      for( i; i < max_i; i += 1 ) {
        if( haystack[i] === needle ) return true;
      }
    }
    else if( iterationType === 'descending' ) {
      for( i; i > max_i; i -= 1 ) {
        if( haystack[i] === needle ) return true;
      }
    }
    
    return false;
  }
  /**
   * [closestParent description]
   * @param  {[type]} element       [description]
   * @param  {[type]} tagNameToFind [description]
   * @param  {[type]} elementToStop [description]
   * @return {[type]}               [description]
   */
  function closestParent(element, tagNameToFind, elementToStop) {
    if( elementToStop && element === elementToStop ) return false;
    else if( element === document.body ) return false;

    if( isArray( tagNameToFind ) ) {
      var elementIsFound = false,
          i = 0, max_i = tagNameToFind.length;

      for( i; i < max_i; i += 1 ) {
        if( element.tagName.toLowerCase() === tagNameToFind[i] ) {
          elementIsFound = true;
          break;
        }
      }
      if( elementIsFound ) return element;
      else return closestParent( element.parentNode, tagNameToFind, elementToStop );
    }
    else if( !(isArray( tagNameToFind )) && typeof tagNameToFind === 'string' ) {
      if( element.tagName.toLowerCase() === tagNameToFind ) return element;
      else return closestParent( element.parentNode, tagNameToFind, elementToStop );
    }
  }
  /**
   * [numberFormat description]
   * discuss at: http://locutus.io/php/number_format/
   * original by: Jonas Raoni Soares Silva (http://www.jsfromhell.com)
   * 
   * @param  {Number} number       [description]
   * @param  {Number} decimals     [description]
   * @param  {String} decPoint     [description]
   * @param  {String} thousandsSep [description]
   * @return {String}              [description]
   */
  function numberFormat(number, decimals, decPoint, thousandsSep) {
    number = (number + '').replace(/[^0-9+\-Ee.]/g, '');
    var n = !isFinite(+number) ? 0 : +number;
    var prec = !isFinite(+decimals) ? 0 : Math.abs(decimals);
    var sep = (typeof thousandsSep === 'undefined') ? ',' : thousandsSep;
    var dec = (typeof decPoint === 'undefined') ? '.' : decPoint;
    var s = '';

    var toFixedFix = function(n, prec) {
      var k = Math.pow(10, prec);
      return '' + (Math.round(n * k) / k).toFixed(prec);
    };

    // @todo: for IE parseFloat(0.55).toFixed(0) = 0;
    s = (prec ? toFixedFix(n, prec) : '' + Math.round(n)).split('.');
    if (s[0].length > 3) {
      s[0] = s[0].replace(/\B(?=(?:\d{3})+(?!\d))/g, sep);
    }
    if ((s[1] || '').length < prec) {
      s[1] = s[1] || '';
      s[1] += new Array(prec - s[1].length + 1).join('0');
    }

    return s.join(dec);
  }
  /**
   * [extendObject description]
   * @param  {[type]} target [description]
   * @param  {[type]} source [description]
   * @return {[type]}        [description]
   */
  function extendObject(target, source) {
    for( var key in source ) {
      if( typeof target[key] === 'undefined' ) {
        target[key] = source[key];
      }
      else if( typeof target[key] === 'object' ) {
        for( var deepKey in source[key] ) {
          if( typeof target[key][deepKey] === 'undefined' ) {
            target[key][deepKey] = source[key][deepKey];
          }
        }
      }
    }
    return target;
  }
  /**
   * [applyPrefixedListener description]
   * @param  {[type]} type [description]
   * @return {[type]}      [description]
   */
  function applyPrefixedListener(type) {
    var webEngines = ["webkit", "moz", "MS", "o", ""];

    for (var p=0; p<webEngines.length; p++) {
      if( !webEngines[p] ) type = type.toLowerCase();
      return webEngines[p]+type;
    }
  }
  /**
   * [titleCaseWords description]
   * @param  {[type]} str [description]
   * @return {[type]}     [description]
   */
  function titleCaseWords(str) {
    return str.replace(/\w\S*/g, function(txt) {
      return txt.charAt(0).toUpperCase() + txt.substr(1).toLowerCase();
    });
  }
  /**
   * [createImageSpinner description]
   * @param  {[type]} size    [description]
   * @param  {[type]} display [description]
   * @return {[type]}         [description]
   */
  function createImageSpinner(size, display) {
    var sizeInvalid = ( size !== 'small' && size !== 'medium' && size !== 'large' ),
        displayInvalid = ( display !== 'block' && display !== 'inline' );
    if( sizeInvalid || displayInvalid ) return;

    var fragment = document.createDocumentFragment(),
        container = document.createElement('span'),
        i = 1, max_i = 3;
    container.className = "rima-lazy-spinner " + size + " " + display;
    container.appendChild( document.createElement('span') );
    var wrapper = container.children[0];
    wrapper.className = "spinner clearfix";
    for( i; i <= max_i; i += 1 ) {
      wrapper.appendChild( document.createElement('span') ).className = "bounce" + i + " pull-left";
    }

    fragment.appendChild(container);

    return fragment;
  }
  /**
   * [explodeURL description]
   * @param  {[type]} urlString [description]
   * @return {[type]}           [description]
   */
  function explodeURL(urlString) {
    var uriArr = urlString.split( app.url.origin )[1].split('/');
    return uriArr.filter( Boolean );
  }
  /**
   * [APIRequest description]
   * @param {[type]} method   [description]
   * @param {[type]} uri      [description]
   * @param {[type]} handlers [description]
   * @param {[type]} timeout  [description]
   */
  function APIRequest(method, uri, handlers, timeout) {
    var xhr = new XMLHttpRequest();
    if( !method || !uri ) return;
    if( typeof handlers !== 'object' ) return;
    method = String(method).toUpperCase();
    if( method !== 'GET' && method !== 'POST' ) {
      console.warn("Unknown request method!");
      return;
    }
    if( typeof uri !== 'string' ) uri = String(uri);

    var url = ( uri.substring(0, 4) !== 'http' ) ? "http://newsapi.rimanews.com/"+ uri : uri;
    if( typeof timeout == 'number' && timeout > 0 ) xhr.timeout = timeout;

    // The fetch initiates
    xhr.onloadstart = function() {
      if( 'onloadstart' in handlers && typeof handlers.onloadstart == 'function' )
        handlers.onloadstart();
    };
    // Transmitting data
    xhr.onprogress = function() {
      if( 'onprogress' in handlers && typeof handlers.onprogress == 'function' )
        handlers.onprogress();
    };
    // The fetch succeeded
    xhr.onload = function() {
      var waitResponsePopulated = setInterval(function() {
        if( xhr.status !== 0 ) {
          clearInterval(waitResponsePopulated);

          if( xhr.status >= 200 && xhr.status < 400 ) {
            var raw = JSON.parse( xhr.responseText );

            if( typeof raw.data !== 'undefined' ) {
              if( isArray( raw.data ) ) {
                xhr.responseData = [];
                for( var key in raw.data ) xhr.responseData.push( raw.data[key] );
              }
              else xhr.responseData = raw.data;
            }
            else xhr.responseData = raw;

            if( 'onload' in handlers && typeof handlers.onload == 'function' ) {
              handlers.onload( xhr.responseData );
            }
          }
          else if( xhr.status >= 400 ) {
            console.error('Failed to get the requested data. Code: ' + xhr.status);
            // call action to hide and show notifitication that data loading has been failed
          }
          // else if( request.getResponse(true) < 0 ) {}
        }
      }, 50);
    };
    // The fetch failed
    xhr.onerror = function() {
      console.log(xhr);
      if( 'onerror' in handlers && typeof handlers.onerror == 'function' )
        handlers.onerror();
    };
    // Timeout has passed before the fetch completed
    xhr.ontimeout = function() {
      console.log('TIMEOUT REACHED!');

      if( 'ontimeout' in handlers && typeof handlers.ontimeout == 'function' )
        handlers.ontimeout();
    };
    // The fetch completed (both for success and failure)
    xhr.onloadend = function() {
      console.log(xhr);
      if( 'onloadend' in handlers && typeof handlers.onloadend == 'function' )
        handlers.onloadend();
    };

    // Set xhr target connection and HTTP header type
    if( 'withCredentials' in xhr ) {
      // XHR for Chrome / Firefox / Opera / Safari
      xhr.open(method, url, true);
    }
    else if( typeof XDomainRequest !== 'undefined' ) {
      // XHR for older IE
      xhr = new XDomainRequest();
      xhr.open(method, url);
    }

    return xhr;
  }
  /**
   * [datetimeToHumanDate description]
   * @param  {[type]} datetime       [description]
   * @return {[type]}                [description]
   */
  function datetimeToHumanDate(datetime) {
    var rawDate = datetime.split('T')[0],
        rawTime = datetime.split('+')[0].split('T')[1].substring(0, 5),
        humanDate = {
          date: moment( rawDate ).format('D'),
          month: moment( rawDate ).format('MMMM'),
          year: moment( rawDate ).format('YYYY')
        },
        makeResponsive = ( typeof arguments[1] === 'boolean' ) ? arguments[1] : false,
        finalDateString = '';

    if( makeResponsive ) {
      if( document.documentElement.lang === 'id' ) {
        if( humanDate.month === 'Agustus' ) {
          humanDate.month = 'Ag<span class="long-date-tail">u</span>s<span class="long-date-tail">tus</span>';
        }
        else if( humanDate.month !== 'Mei' ) {
          humanDate.month = humanDate.month.substring(0, 3) + '<span class="long-date-tail">' + humanDate.month.substring(3) + '</span>';
        }

        finalDateString = humanDate.date + ' ' + humanDate.month + ' ' + humanDate.year;
      }
      else if( document.documentElement.lang === 'en' ) {
        if( humanDate.month !== 'May' ) {
          humanDate.month = humanDate.month.substring(0, 3) + '<span class="long-date-tail">' + humanDate.month.substring(3) + '</span>';
        }

        finalDateString = humanDate.month + ' ' + humanDate.date + ', ' + humanDate.year;
      }
    }

    return finalDateString;
  }
  /**
   * [debounce description]
   * @param  {[type]} func      [description]
   * @param  {[type]} wait      [description]
   * @param  {[type]} immediate [description]
   * @return {[type]}           [description]
   */
  function debounce(func, wait, immediate) {
    var timeout;
    return function() {
      var context = this,
          args = arguments;
      var later = function() {
        timeout = null;
        if (!immediate) func.apply(context, args);
      };
      var callNow = immediate && !timeout;
      clearTimeout(timeout);
      timeout = setTimeout(later, wait);

      if (callNow) func.apply(context, args);
    };
  }

  /////////////////////////////
  // Rimanews Application JS //
  /////////////////////////////
  /**
   * Rimanews Web App constructor
   * @param {[type]} container [description]
   * @param {[type]} params    [description]
   */
  var RimanewsWebApp = function(container, params) {
    if( !(this instanceof RimanewsWebApp) ) return new RimanewsWebApp(container, params);

    var app = this;
    var defaults = {
      // Properties
      landingHeader: 'RIMANEWS',
      rubricsHeader: 'RUBRICS',
      readHeader: 'READ NEWS',
      searchHeader: '',
      sharingHeader: 'SHARE TO',
      // Pages Container ID
      topbarContainerId: 'app-master-topbar',
      landingContainerId: 'landing',
      rubricsContainerId: 'rubrics',
      readContainerId: 'read',
      searchContainerId: 'search',
      sharingContainerId: 'sharing',
      // Classname
      newsContentClass: 'news-content',
      articlePrefixClass: 'article__',
      pageTogglerClass: 'component-toggler',
      activeClass: 'active',
      showClass: 'show',
      hiddenClass: 'hidden',
      // Callback
    };

    // Extend the user-configured params into default configuration
    params = params || {};
    app.params = extendObject(params, defaults);
    params = defaults = null;
    // Assign container as property
    app.container = document.getElementById( container.substring(1) );
    container = null;
    // Add / prepare another important properties
    app.containerWidth = app.container.clientWidth;
    app.containerHeight = app.container.clientHeight;
    app.activePage = app.container.dataset.activePage;
    app.activeNews = null;
    app.landing = null;
    app.rubrics = null;
    app.read = null;
    app.search = null;
    app.url = {
      protocol: document.location.protocol,
      hostname: document.location.hostname,
      origin: document.location.origin,
      path: document.location.pathname,
      hash: document.location.hash
    };
    app.isDesktop = ( document.documentElement.classList.contains('desktop') );

    //////////////////////////////
    // Events & Callback Caller //
    //////////////////////////////
    // @deprecated
    /**
     * [emit description]
     * @param  {[type]} eventName [description]
     * @return {[type]}           [description]
     */
    app.emit = function (eventName) {
      // Trigger callbacks
      if(app.params[eventName]) {
        app.params[eventName](arguments[1], arguments[2], arguments[3], arguments[4], arguments[5]);
      }
      // Trigger events
      // if(app.emitterEventListeners[eventName]) {
      //   for(var i=0; i<app.emitterEventListeners[eventName].length; i++) {
      //     app.emitterEventListeners[eventName][i](arguments[1], arguments[2], arguments[3], arguments[4], arguments[5]);
      //   }
      // }
    };
    // @obsolete
    /**
     * [eventPrevention description]
     * @param  {[type]} event [description]
     * @return {[type]}       [description]
     */
    app.eventPrevention = function(event) {
      event = event || window.event;
      return event.preventDefault();
    };
    // @obsolete
    /**
     * [eventStopPropagation description]
     * @param  {[type]} event [description]
     * @return {[type]}       [description]
     */
    app.eventStopPropagation = function(event) {
      event = event || window.event;
      return event.stopPropagation ? event.stopPropagation() : (event.cancelBubble=true);
    };

    // @obsolete
    ////////////
    // Slides //
    ////////////
    /**
     * [getRelatedNews description]
     * @param  {[type]} data [description]
     * @return {[type]}      [description]
     */
    // Slides.prototype.getRelatedNews = function(data) {
    //   var i = 0, max = data.length, request = [];

    //   for( i; i < max; i += 1 ) {
    //     var apiTarget = '/getnewsrelated/' + data[i].hash + '/0/3';
    //     request.push( new APIRequest('GET', apiTarget, {}) );
    //     request[i].send(null);
    //     // @todo check null assignment
    //     // request[i] = null;
    //   }

    //   return request;
    // };

    /////////////
    // Article //
    /////////////
    function Article(id, isSponsor, container) {
      // Initialize properties
      this.id = id;
      this.isSponsor = isSponsor;
      this.container = container;
      this.scrollPos = 0;
      this.touchStartPos = 0;
      this.touchCurrentPos = 0;

      // Initialize preloader rendering
      var preloader = (function() {
        var fragment = document.createDocumentFragment(),
            container = document.createElement('div'),
            i, j;

        // Create container
        container.className = "article-preloader clearfix";
        container.appendChild( document.createElement('div') ); // header preloader container
        container.appendChild( document.createElement('div') ); // body preloader container

        // Create preloader header
        var header = container.children[0];
        header.className = "ap-header col-xs-12 col-lg-10 col-lg-offset-1";
        header.appendChild( document.createElement('div') ); // header rubrics container
        header.appendChild( document.createElement('span') ); // header title
        header.appendChild( document.createElement('div') ); // header meta container
        // Create rubrics on header
        var hRubrics = header.children[0];
        hRubrics.className = "ap-rubrics clearfix";
        for( i = 0; i < 3; i += 1 ) {
          hRubrics.appendChild( document.createElement('span') );
          if( i == 1 ) hRubrics.children[i].innerHTML = "&#8226;";
        }
        // Create title on header
        var hTitle = header.children[1];
        hTitle.className = "ap-title";
        // Create meta post on header
        var hMeta = header.children[2];
        hMeta.className = "ap-meta clearfix";
        for( i = 0; i < 2; i += 1) {
          hMeta.appendChild( document.createElement('div') );
          hMeta.children[i].className = (i === 0) ? "apm-l pull-left" : "apm-r pull-left";
          for( j = 0; j < 2; j += 1 ) {
            hMeta.children[i].appendChild( document.createElement('span') );
          }
        }

        // Create preloader body
        var body = container.children[1];
        body.className = "ap-body col-xs-12 col-lg-10 col-lg-offset-1";
        body.appendChild( document.createElement('span') ); // body image container
        body.appendChild( document.createElement('div') ); // body article container
        // Create heading imange on body
        var bImage = body.children[0];
        bImage.className = "apb-image";
        // Create article on body
        var bArticle = body.children[1];
        bArticle.className = "apb-paragraph";
        for( i = 0; i < 5; i += 1 ) {
          bArticle.appendChild( document.createElement('span') );
        }

        // Combine all virtual DOM into fragment
        fragment.appendChild( container );
        // And return as assignment to variable
        return fragment;
      })();
      this.container.appendChild( preloader );
      preloader = null;

      return this;
    }
    Article.prototype.destroy = function() {
      while( this.container.lastElementChild ) {
        this.container.removeChild( this.container.lastElementChild );
      }

      delete this.id;
      delete this.container;
      delete this.scrollPos;
      delete this.touchStartPos;
      delete this.touchCurrentPos;
      return null;
    };
    Article.prototype.render = function(contentData, callback) {
      var fragment = document.createDocumentFragment(),
          i, max_i;

      fragment.appendChild( document.createElement('header') ); // 1st child, header
      fragment.appendChild( document.createElement('div') ); // 2nd child, news entry
      fragment.appendChild( document.createElement('footer') ); // 3rd child, footer

      // Create article header
      var header = fragment.children[0];
      header.className = 'col-xs-12 col-sm-12 col-lg-10 col-lg-offset-1 news-header';
      header.appendChild( document.createElement('div') ); // 1st child, rubrics
      header.appendChild( document.createElement('h2') ); // 2nd child, news title
      header.appendChild( document.createElement('div') ); // 3rd child, metapost
      // Create rubrics label
      var rubrics = header.children[0];
      rubrics.className = 'news-rubrics';
      rubrics.appendChild( document.createElement('div') );
      var rubricsWrapper = rubrics.children[0];
      rubricsWrapper.className = 'rubrics-wrapper clearfix';
      for( i = 0; i <= 2; i += 1 ) {
        if(i === 0 || i === 2) {
          var text = (i === 0) ? contentData.rubric.main : contentData.rubric.sub;

          if( text !== '' && typeof text === 'string' )
            rubricsWrapper.innerHTML += '<a class="pull-left" href="#!">'+ titleCaseWords(text) +'</a>';
        }
        else {
          if( contentData.rubric.sub !== '' && typeof contentData.rubric.sub !== 'undefined' )
            rubricsWrapper.innerHTML += '<span class="pull-left bullet">&#8226;</span>';
          else break;
        }
      }
      // Create title
      var title = header.children[1];
      title.className = 'news-title';
      title.innerHTML = contentData.news.title;
      // Create metapost
      var metapost = header.children[2];
      metapost.className = 'news-meta clearfix';
      metapost.appendChild( document.createElement('div') ); // 1st child, date
      metapost.appendChild( document.createElement('div') ); // 2nd child, reporter
      for( i = 0; i <= 1; i += 1 ) {
        var metapostEntry = metapost.children[i];
        metapostEntry.className = 'pull-left meta-entry ' + ( (i === 0) ? 'post-date' : 'reporter' );
        metapostEntry.appendChild( document.createElement('span') ).className = 'meta-label';
        var metapostLabel = metapostEntry.children[0];
        if(i === 0) {
          metapostLabel.innerText = 'PUBLISHED ON:';
          // @future: create more deeper nodes for meta time and make it responsive
          metapostEntry.innerHTML += '<time class="meta-value" datetime="'+ contentData.date +'">'+ datetimeToHumanDate( contentData.date, true ) +'</time>';
        }
        else {
          metapostLabel.innerText = 'REPORTED BY:';
          // @future: create reporter photo markup
          metapostEntry.innerHTML += '<span class="meta-value">'+ titleCaseWords(contentData.reporter.name) +'</span>';
        }
      }

      // Create article news entry
      var newsEntry = fragment.children[1];
      newsEntry.className = 'col-xs-12 col-lg-10 col-lg-offset-1 news-entry';
      newsEntry.appendChild( document.createElement( 'p' ) );
      // Create news heading image
      var headingImage = newsEntry.children[0];
      headingImage.className = 'row';
      headingImage.appendChild( document.createElement('span') ); // 1st child, figure wrapper
      if( typeof contentData.cover.background.desc !== 'undefined' && contentData.cover.background.desc !== "" ) {
        headingImage.appendChild( document.createElement('span') ); // 2nd` child, caption wrapper
      }
      var figure = headingImage.children[0];
      figure.className = "figure figure-headline";
      figure.appendChild( document.createElement('img') ); // img tag
      figure.children[0].className = 'b-lazy';
      figure.children[0].src = 'data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAABAAAAAJCAQAAACRI2S5AAAAEElEQVR42mNkIAAYRxWAAQAG9gAKqv6+AwAAAABJRU5ErkJggg==';
      figure.children[0].dataset.src = encodeURI( contentData.cover.background.url );
      figure.appendChild( createImageSpinner('large', 'block') );
      if( typeof contentData.cover.background.desc !== 'undefined' && contentData.cover.background.desc !== "" ) {
        var caption = headingImage.children[1];
        caption.className = "caption caption-headline";
        caption.innerText = contentData.cover.background.desc;
      }
      // Create paragraphs
      newsEntry.innerHTML += contentData.news.article;
      // Attempt to remove Rimanews copyright flag
      var rimaFlag = newsEntry.children[1].childNodes[0];
      if( rimaFlag && rimaFlag.nodeName.toLowerCase() === 'strong' ) {
        newsEntry.children[1].removeChild( rimaFlag );
        rimaFlag = null;
        // Attempt to remove whitespace and dash that was left by the removed flag
        var flagGarbage = newsEntry.children[1].innerHTML.substring(0, 3),
            garbageCount = 0;
        for( i = 0; i < 3; i += 1 ) {
          // count for space and dash
          if( flagGarbage.charAt(i) === ' ' || flagGarbage.charAt(i) == '-' )
            garbageCount += 1;
          // count for &nbsp;
          else if( flagGarbage.charAt(i) == '&' )
            garbageCount += 6;
        }
        if( garbageCount > 0 )
          newsEntry.children[1].innerHTML = newsEntry.children[1].innerHTML.substring(garbageCount);
      }
      // Check for inner figures or video and fix the outbound positioning
      // i counter start from 2 because <p>[0] is heading image and <p>[1] is
      // opening paragraph.
      var paragraphLength = newsEntry.children.length;
      for( i = 2; i < paragraphLength; i += 1 ) {
        var pTag = newsEntry.children[i];
        if( pTag.children.length === 1 &&
            (
              pTag.children[0].tagName == 'IMG' ||
              pTag.children[0].tagName == 'IFRAME' ||
              pTag.children[0].tagName == 'VIDEO'
            )
          ) {
          pTag.className = 'row';
          // pTag.children[0].setAttribute('style', 'width: 100%; height: 100%;');
        }
        else continue;
      }
      // Create related news section
      if( contentData.news.related.length > 0 ) {
        var related = document.createElement('section');
        related.className = 'related-news';
        related.appendChild( document.createElement('h3') ).innerText = 'You might also like:'; 
        related.appendChild( document.createElement('ul') ); // 2nd children, listing
        var relatedListing = related.children[1];
        for( i = 0, max_i = contentData.news.related.length; i < max_i; i += 1 ) {
          relatedListing.appendChild( document.createElement('li') );
          relatedListing.children[i].appendChild( document.createElement('a') );
          var relatedAnchor = relatedListing.children[i].children[0];
          relatedAnchor.href = app.url.origin + contentData.news.related[i].href.substring(19);
          relatedAnchor.className = 'js-prevent';
          relatedAnchor.dataset.hash = contentData.news.related[i].hash;
          relatedAnchor.dataset.title = contentData.news.related[i].title;
          relatedAnchor.innerHTML = contentData.news.related[i].title;
          relatedAnchor.onclick = app.read.onNewsItemClick( relatedAnchor, true );
        }
        if( paragraphLength >= 3 && paragraphLength <= 5 ) {
          newsEntry.appendChild( related );
        }
        else if( paragraphLength >= 6 ) {
          newsEntry.insertBefore( related, newsEntry.children[4] );
        }
      }

      // @future: create video and set conditional statement to set its position
      // (top or bottom video)

      // Create article footer / subscribe
      var footer = fragment.children[2];
      footer.className = 'news-footer col-xs-12 col-sm-12 col-md-12 col-lg-12';
      footer.appendChild( document.createElement('div') );  // 1st child, subscribe intro
      footer.appendChild( document.createElement('div') );  // 2nd child, form wrapper
      footer.appendChild( document.createElement('p') );    // 3th child, subscribe outro
      var introText = footer.children[0];
      introText.className = "row";
      introText.appendChild( document.createElement('p') );
      introText = introText.children[0];
      introText.className = "subscribe-intro col-xs-8 col-xs-offset-2";
      introText.innerHTML = '<span>' + 
                              '<span>Stay up to date with&nbsp;</span>' +
                              '<i class="logo-text">Rimanews</i>' +
                            '</span>' +
                            '<span>Get the latest news alerts delivered to your inbox</span>';
      var formWrapper = footer.children[1];
      formWrapper.className = "row";
      formWrapper.appendChild( document.createElement('div') );
      formWrapper = formWrapper.children[0];
      formWrapper.className = "subscribe-form-wrapper clearfix";
      formWrapper.appendChild( document.createElement('input') );  
      formWrapper.appendChild( document.createElement('button') );
      var emailField = formWrapper.children[0];
      emailField.setAttribute('type', 'email');
      emailField.setAttribute('name', 'subscriber_email');
      emailField.setAttribute('placeholder', 'Your email address');
      var subscribeButton = formWrapper.children[1];
      subscribeButton.className = 'subscribe-button guest-mode';
      subscribeButton.setAttribute('type', 'button');
      subscribeButton.innerText = 'Subscribe Now';
      subscribeButton.addEventListener('click', app.subscribe.send, false);
      var outroText = footer.children[2];
      outroText.className = "subscribe-outro";
      outroText.innerText = "Don't worry we won't spam you";

      this.container.appendChild( fragment );

      if( typeof callback === 'function' ) callback();
    };
    Article.prototype.fetchContent = function(callback) {
      var context = this,
          apiTarget = ( !this.isSponsor ) ? '/getnewsbyid/'+ this.id : '/getsponsorbyid/'+ this.id;
      var request = APIRequest('GET', apiTarget, {
        onload: function(response) {
          context.render(response[0], function() {
            context.container.removeChild( context.container.firstElementChild );
            if( typeof callback === 'function' ) callback();
          });
        }
      });

      request.send(null);
      request = null;
    };

    // @todo: improve history manipulation
    ////////////////////
    // History Logger //
    ////////////////////
    function RHL() {
      this.activeIndex = 0;
      this.previousIndex = -1;
      this.states = [];

      return this;
    }
    /**
     * [setHistoryStateObject description]
     */
    function setHistoryStateObject(documentTitle) {
      // console.log( app.rhl.states );
      return {
        topbarTogglers: (function() {
          var togglers = [],
              i = 0, max_i = app.topbar.togglers.length;

          for( i; i < max_i; i += 1 ) {
            togglers.push({
              uri: ( typeof app.topbar.togglers[i].href !== 'undefined' ) ? app.topbar.togglers[i].href.split( app.url.origin )[1] : null,
              pageTarget: app.topbar.togglers[i].dataset.pageTarget,
              iconClass: app.topbar.togglers[i].children[0].className.split(' ')[1],
              visible: ( !( app.topbar.togglers[i].parentNode.classList.contains( app.params.hiddenClass ) ) ) ? true : false
            });
          }

          return togglers;
        }()),
        topbarClass: app.topbar.container.className,
        activePage: app.activePage,
        previousPage: ( app.rhl.previousIndex > -1 ) ? app.rhl.states[ app.rhl.previousIndex ].activePage : null,
        documentTitle: generateDocumentTitle(documentTitle)
      };
    }
    function generateDocumentTitle(documentTitle) {
      var flag = 'Rimanews.com',
          separator = '|',
          title;

      switch( app.activePage ) {
        case 'landing':
          title = flag;
        break;

        case 'read':
          // create virtual DOM to access the decoded value of documentTitle param
          var txt = document.createElement("textarea");
          txt.innerHTML = documentTitle;
          title = flag + ' ' + separator + ' ' + txt.value;
        break;

        case 'rubrics':
          title = flag + ' ' + separator + ' Rubrics';
        break;

        case 'search':
          title = flag + ' ' + separator + ' Search';
        break;
      }

      return title;
    }
    /**
     * [add description]
     */
    RHL.prototype.add = function(documentTitle) {
      if( this.states.length > this.activeIndex + 1 ) {
        this.states.splice( this.activeIndex + 1 );
      }
      this.activeIndex += 1;
      this.previousIndex += 1;
      this.states.push( setHistoryStateObject(documentTitle) );
      window.history.pushState(
        this.states[ this.activeIndex ],
        this.states[ this.activeIndex ].documentTitle,
        app.url.path
      );
      document.title = this.states[ this.activeIndex ].documentTitle;
      // console.log(window.history);
      // console.log(this);
    };
    /**
     * [replace description]
     * @return {[type]} [description]
     */
    RHL.prototype.replace = function(documentTitle) {
      // console.log(app.url);
      this.states[ this.activeIndex ] = setHistoryStateObject(documentTitle);
      window.history.replaceState(
        this.states[ this.activeIndex ],
        this.states[ this.activeIndex ].documentTitle,
        app.url.path
      );
      document.title = this.states[ this.activeIndex ].documentTitle;
      // this.activeIndex += 1;
      // this.previousIndex += 1;
      // console.log(window.history);
      // console.log(this);
    };
    /**
     * [moveBack description]
     * @return {[type]} [description]
     */
    RHL.prototype.moveBack = function() {
      window.history.back();
      this.activeIndex -= 1;
      this.previousIndex -= 1;
      document.title = this.states[ this.activeIndex ].documentTitle;
      // app[ app.rhl.states[ app.rhl.activeIndex ].activePage ].setActive();
    };
    /**
     * [onPopState description]
     * @param  {[type]} event [description]
     * @return {[type]}       [description]
     */
    window.onpopstate = function(event) {};

    // RHL.prototype.moveForward = function() {
    //   this.states.push( setHistoryStateObject() );
    //   window.history.pushState(
    //     this.states[ this.activeIndex ].component,
    //     this.states[ this.activeIndex ].docTitle,
    //     this.states[ this.activeIndex ].docURL
    //   );
    //   this.activeIndex += 1;
    //   this.previousIndex += 1;
    // };
    /**
     * [onPopState description]
     * @return {[type]} [description]
     */
    // RHL.prototype.onPopState = function() {};
    // window.onpopstate = function(event) {};

    ///////////
    // Pages //
    ///////////
    var _page;

    function Page(container, params) {
      this.container = container;
      return extendObject(this, params);
    }
    Page.prototype.beforeActivated = function() {
      // app.activePage = 
    };
    Page.prototype.afterActivated = function() {};
    Page.prototype.fragments = {
      landing: {
        swipeHint: (function() {}()),
        cover    : function(data) {
          // Create cover container
          var container = document.createElement('div');
          container.className = 'swiper-slide news-item-cover';
          container.appendChild( document.createElement('div') ); // 1st child, background
          container.appendChild( document.createElement('div') ); // 2nd child, title

          // Crate cover background
          var background = container.children[0],
              backgroundType = ( data.cover.background.type.toLowerCase() ) ? 'image' : 'video',
              backgroundNode = ( backgroundType == 'image' ) ? 'img' : 'video';
          background.className = 'background-wrapper';
          background.id = data.hash + '-bg-canvas';
          background.appendChild( document.createElement(backgroundNode) );
          backgroundNode = background.children[0];
          backgroundNode.id = data.hash + '-bg-' + backgroundType;
          if( backgroundType == 'image' ) {
            backgroundNode.className = 'b-lazy';
            backgroundNode.src = 'data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAABAAAAAJCAQAAACRI2S5AAAAEElEQVR42mNkIAAYRxWAAQAG9gAKqv6+AwAAAABJRU5ErkJggg==';
            backgroundNode.dataset.src = encodeURI( data.cover.background.url );
            backgroundNode.alt = data.cover.title;
          }
          else if( backgroundType == 'video' ) {
            backgroundNode.preload = 'metadata';
            backgroundNode.loop = 'loop';
            backgroundNode.poster = '';
            // @future: add source element here
          }

          // Create cover title
          var title = container.children[1],
              titleLength = data.cover.title.length;
          title.id = data.hash + '-title';
          if( titleLength < 30 ) {
            title.className = 'cover-title bottom font-xlarge';
          }
          else if( titleLength >= 30 && titleLength <= 50 ) {
            title.className = 'cover-title bottom font-large';
          }
          else if( titleLength > 50 ) {
            title.className = 'cover-title bottom font-normal';
          }
          title.appendChild( document.createElement('p') );
          var titleNode = title.children[0];
          if( !data.cover.subtitle ) {
            titleNode.className = 'title-heading no-paragraph-margin';
            titleNode.innerHTML = data.cover.title;
          }
          // Create read button if users' device is desktop
          if( app.isDesktop ) {
            title.appendChild( document.createElement('button') );
            var readButton = title.children[1];
            readButton.id = data.hash +'-read-button';
            readButton.className = "read-this-button";
            readButton.type = "button";
            readButton.innerHTML = "Continue reading ...";
          }
          // @future: else, create cover title with subtitle

          container.appendChild(background);
          container.appendChild(title);

          // Create cover swipe hint
          if( !app.isDesktop ) {
            container.appendChild( document.createElement('div') ); // 3rd child of container, swipe hint
            var swipeHint = container.children[2];
            swipeHint.className = 'col-xs-12 news-slide-hint';
            // swipeHint.innerHTML = _this.statics.swipeHint;
            container.appendChild(swipeHint);
          }

          return container;
        },
        article  : function() {
          var fragment = document.createDocumentFragment(),
              container = document.createElement('div');
          container.className = 'swiper-slide news-item-article';
          container.appendChild( document.createElement('article') ); // 1st child, article
          
          var article = container.children[0];
          article.className = 'clearfix';

          fragment.appendChild( container );

          return fragment;
        },
        preloader: function() {
          var fragment = document.createDocumentFragment(),
              slide = document.createElement('div');
          slide.className = 'swiper-slide';
          slide.appendChild( document.createElement('div') );

          var container = slide.children[0];
          container.className = "swiper-container preloader-content";
          container.appendChild( document.createElement('div') );

          var wrapper = container.children[0];
          wrapper.className = "swiper-wrapper";
          wrapper.appendChild( document.createElement('div') );

          var loader = wrapper.children[0];
          loader.className = "loader";
          loader.appendChild( document.createElement('i') );
          loader.appendChild( document.createElement('span') );

          var spinner = loader.children[0];
          spinner.className = "spinner";
          var spinnerLabel = loader.children[1];
          spinnerLabel.className = "status";
          spinnerLabel.innerText = "Fetching news ...";

          fragment.appendChild( slide );

          return fragment;
        }
      },
      read: null,
      rubrics: {
        spinner  : (function() {
          // Create spinner container
          var fragment = document.createDocumentFragment(),
              container = document.createElement('div');
          container.id = 'spinner';
          container.className = app.params.hiddenClass;
          container.style.visibility = 'hidden';
          container.appendChild( document.createElement('div') ); // 1st children, overlay
          container.appendChild( document.createElement('div') ); // 2nd children, icon wrapper
          // Create overlay
          var overlay = container.children[0];
          overlay.id = "spinner-overlay";
          // Create spinner icon
          var iconWrapper = container.children[1];
          iconWrapper.id = "spinner-icon-container";
          iconWrapper.style.transform = "translateY(-"+ String( _spinnerInitTranslateY ) +"px) scale(0)";
          iconWrapper.style.transition = 'transform 325ms';
          iconWrapper.appendChild( document.createElement('i') ).className = "iconorima icon-close";
          // Append container into document fragment and return it as an object
          fragment.appendChild( container );
          container = null;
          return fragment;
        }()),
        category : function(data) {
          // Create category fragment
          var fragment = document.createDocumentFragment(),
              container = document.createElement('ul'),
              _categories = [],
              _imagesPath = "/public/images/rubrics/",
              _imagesExt = ".png",
              _imagesName = [
                'todays',
                'politics3',
                'ideas3',
                'living2',
                'lifestyle4'
              ],
              i = 0, max_i = data.length;
          // Start looping
          for( i; i < max_i; i += 1 ) {
            container.appendChild( document.createElement('li') );
            // Create list item
            var list = container.children[i];
            list.appendChild( document.createElement('a') );
            // Create anchor
            var anchor = list.children[0];
            anchor.className = 'js-prevent';
            anchor.href = '/rubrics/' + data[i].uri;
            anchor.dataset.rubricsId = String(data[i].id);
            anchor.appendChild( document.createElement('img') ); // 1st child, image
            // anchor.appendChild( document.createElement('span') ); // 2nd child, lazy spinner
            anchor.appendChild( createImageSpinner('small', 'block') );
            anchor.appendChild( document.createElement('span') ); // 3nd child, cover
            // Create list image
            var image = anchor.children[0];
            image.className = "b-lazy";
            image.src = "data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAABAAAAAJCAQAAACRI2S5AAAAEElEQVR42mNkIAAYRxWAAQAG9gAKqv6+AwAAAABJRU5ErkJggg==";
            image.dataset.src = _imagesPath + _imagesName[i] + _imagesExt;
            // image.setAttribute(
            //   'srcset',
            //   _imagesPath + _imagesName[i] + '@3.5x' + _imagesExt + ' 2x, ' +
            //   _imagesPath + _imagesName[i] + '@3.5x' + _imagesExt + ' 3x'
            // );
            // Create lazy spinner
            // var lazy = anchor.children[1];
            // lazy.className = "rima-lazy-spinner small block";
            // lazy.appendChild( document.createElement('span') );
            // lazy = lazy.children[0];
            // lazy.className = "spinner clearfix";
            // lazy.appendChild( document.createElement('span') );
            // lazy.appendChild( document.createElement('span') );
            // lazy.appendChild( document.createElement('span') );
            // for( var j = 1; j <= 3; j += 1 ) {
            //   lazy.children[j-1].className = "bounce" + String(j) + " pull-left";
            // }
            // Create category cover
            var cover = anchor.children[2];
            cover.className = 'overlay-accent ' + data[i].accent;
            cover.appendChild( document.createElement('span') ).className = 'overlay-label';
            cover.children[0].innerText = data[i].title;
            // Store anchor node into array
            _categories.push(anchor);
          }
          // Append container into document fragment
          fragment.appendChild(container);
          container = null;

          return {
            categoriesArray: _categories,
            htmlFragment: fragment
          };
        },
        listings : function(data) {
          var sortedData = [],
              _listings = [],
              fragment = document.createDocumentFragment();

          for( var section in data ) {
            if( section === 'popular' ) {
              sortedData[1] = data[section];
            }
            else if( section === 'newest' ) {
              sortedData[0] = data[section];
            }
            else if( section === 'sponsor' ) {
              for( var sponsorIndex in data.sponsor ) {
                sortedData[sponsorIndex].splice( 1, 0, data.sponsor[sponsorIndex] );
                sortedData[sponsorIndex][1].featured = true;
                sortedData[sponsorIndex][1].rubric.main = "Sponsor";
              }
            }
          }

          for( var i = 0, max_i = sortedData.length; i < max_i; i += 1 ) {
            // Create container and listings inside it
            var sectionName = (i === 0) ? 'newest' : 'popular',
                listWrapper = document.createElement('ul');
            listWrapper.className = (i === 0) ? "category-section" : "category-section hidden";
            listWrapper.appendChild( this.newsItems( sortedData[i], sectionName ) );
            // Append to document fragment and store it as array
            fragment.appendChild(listWrapper);
            _listings.push(listWrapper);
          }

          return {
            listingsArray: _listings,
            htmlFragment: fragment
          };
        },
        newsItems: function(data) {
          var fragment = document.createDocumentFragment(),
              i = 0, max_i = data.length;

          console.log(data);

          for( i; i < max_i; i += 1 ) {
            // Create list
            var itemData = data[i],
                list = document.createElement('li'),
                isFeatured = ( typeof itemData.featured !== 'undefined' ) ? JSON.parse( itemData.featured ) : false,
                isExternal = ( isFeatured && itemData.external ) ? true : false;
            list.className = ( !isFeatured ) ? "regular clearfix" : "featured clearfix";
            list.appendChild( document.createElement('a') );
            // Create anchor
            var anchor = list.children[0];
            if( !isFeatured ) {
              anchor.href = itemData.href.split('rimanews.com')[1];
            }
            else {
              if( !isExternal ) {
                anchor.href = itemData.href.split('rimanews.com')[1];
              }
              else {
                anchor.href = itemData.href;
              }
            }
            anchor.dataset.hash = itemData.hash;
            anchor.dataset.title = itemData.title;
            if( !isFeatured ) {
              anchor.target = '_self';
            }
            else {
              if( itemData.external ) {
                anchor.target = '_blank';
              }
              else {
                anchor.target = '_self';
              }
            }
            // Create news image
            var imageWrapper = document.createElement('div'),
                image = imageWrapper.appendChild( document.createElement('img') );
            imageWrapper.className = "image-box";
            imageWrapper.appendChild(image);
            image.className = "b-lazy";
            image.src = "data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAABAAAAAJCAQAAACRI2S5AAAAEElEQVR42mNkIAAYRxWAAQAG9gAKqv6+AwAAAABJRU5ErkJggg==";
            image.dataset.src = itemData.cover;
            imageWrapper.appendChild( createImageSpinner('small', 'block') );
            // Prepare rubric category label
            var categoryLabel = document.createElement('span');
            categoryLabel.className = "category-label";
            categoryLabel.innerHTML =
              ( !isFeatured ) ?
                itemData.rubric.main.charAt(0).toUpperCase() + itemData.rubric.main.substring(1) :
                itemData.rubric.main;
            // Prepare news title
            var title = document.createElement('h2');
            title.innerHTML = itemData.title;
            // Prepare news publication date
            // @todo: fix date text and attribute
            var _date = itemData.date.split('T')[0],
                _time = itemData.date.split('+')[0].split('T')[1].substring(0,5),
                publishDate = document.createElement('time');
            publishDate.setAttribute("datetime", itemData.date);
            publishDate.innerHTML = ( document.documentElement.lang === 'id' ) ?
                                    moment(_date).format('D MMMM YYYY') :
                                    moment(_date).format('MMMM D, YYYY');

            if( !isFeatured ) {
              var leftSide = document.createElement('div'),
                  rightSide = document.createElement('div');
              // Prepare left side of list row
              leftSide.className = "left-hand pull-left";
              leftSide.appendChild( imageWrapper );
              leftSide.appendChild( categoryLabel );
              // Prepare right side of list row
              rightSide.className = "right-hand pull-left";
              rightSide.appendChild( title );
              rightSide.appendChild( publishDate );
              // Merge the left and right hand into new list element
              anchor.appendChild(leftSide);
              anchor.appendChild(rightSide);
            }
            else { // is featured
              anchor.appendChild( categoryLabel );
              anchor.appendChild( title );
              anchor.appendChild( publishDate );
              anchor.appendChild( imageWrapper );
            }

            // Append anchor to list container
            list.appendChild(anchor);
            // And append the prepared list to document fragment
            fragment.appendChild(list);
          }

          return fragment;
        }
      },
      search: {
        spinner  : (function() {
          var fragment = document.createDocumentFragment(),
              spinner = document.createElement('div');
          spinner.id = 'search-spinner';
          spinner.innerHTML = '<i class="spinner"></i><span class="status">Loading ...</span>';
          
          fragment.appendChild(spinner);

          return fragment;
        }()),
        topics   : function(data) {
          var fragment = document.createDocumentFragment(),
              i = 0, max = data.length;
          
          for( i; i < max; i += 1 ) {
            var list = document.createElement('li');
            list.className = 'animated';
            list.style.visibility = 'hidden';
            list.appendChild( document.createElement('div') );
            
            var wrapper = list.children[0];
            wrapper.className = 'tag-wrapper';
            wrapper.appendChild( document.createElement('a') );
            
            var anchor = wrapper.children[0];
            anchor.className = 'js-prevent clearfix';
            anchor.href = '/search/tag/' + encodeURIComponent( data[i].tag ).toLowerCase();
            anchor.dataset.tagUri = (function() {
              var uriChunks = data[i].href.split('/');
              return uriChunks[ uriChunks.length - 1 ];
            }());
            anchor.appendChild( document.createElement('h3') ).innerText = data[i].tag;
            anchor.appendChild( document.createElement('p') ).innerText = 'Has been shown ' + String( numberFormat( data[i].reputation, 0, '', ',' ) ) + ' times';

            fragment.appendChild( list );
          }

          return fragment;
        },
        listing  : function(data) {
          var fragment = document.createDocumentFragment(),
              i = 0, max_i = data.length;

          for( i; i < max_i; i += 1 ) {
            // Create list
            var itemData = data[i],
                list = document.createElement('li'),
                isFeatured = (typeof itemData.featured !== 'undefined') ? JSON.parse( itemData.featured ) : false;
            list.className = (isFeatured) ? "featured clearfix" : "regular clearfix";
            list.appendChild( document.createElement('a') );
            // Create anchor
            var anchor = list.children[0];
            anchor.href = itemData.href.split('rimanews.com')[1];
            // anchor.className = (isFeatured) ? 'col-xs-12 no-col-side-padding' : 'col-xs-12';
            anchor.dataset.hash = itemData.hash;
            anchor.dataset.title = itemData.title;
            // Create news image
            var imageWrapper = document.createElement('div'),
                image = imageWrapper.appendChild( document.createElement('img') );
            imageWrapper.className = "image-box";
            imageWrapper.appendChild(image);
            imageWrapper.appendChild( createImageSpinner('small', 'block') );
            image.className = "b-lazy";
            image.src = "data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAABAAAAAJCAQAAACRI2S5AAAAEElEQVR42mNkIAAYRxWAAQAG9gAKqv6+AwAAAABJRU5ErkJggg==";
            image.dataset.src = itemData.cover;
            // Prepare rubric category label
            var categoryLabel = document.createElement('span');
            categoryLabel.className = "category-label";
            categoryLabel.innerHTML = itemData.rubric.main.charAt(0).toUpperCase() + itemData.rubric.main.substring(1);
            // Prepare news title
            var title = document.createElement('h3');
            title.innerHTML = itemData.title;
            // Prepare news publication date
            // @todo: fix date text and attribute
            var _date = itemData.date.split('T')[0],
                _time = itemData.date.split('+')[0].split('T')[1].substring(0,5),
                publishDate = document.createElement('time');
            publishDate.setAttribute("datetime", itemData.date);
            publishDate.innerHTML = ( document.documentElement.lang === 'id' ) ?
                                    moment(_date).format('D MMMM YYYY') :
                                    moment(_date).format('MMMM D, YYYY');

            if( !isFeatured ) {
              var leftSide = document.createElement('div'),
                  rightSide = document.createElement('div');
              // Prepare left side of list row
              leftSide.className = "left-hand pull-left";
              leftSide.appendChild( imageWrapper );
              leftSide.appendChild( categoryLabel );
              // Prepare right side of list row
              rightSide.className = "right-hand pull-left";
              rightSide.appendChild( title );
              rightSide.appendChild( publishDate );
              // Merge the left and right hand into new list element
              anchor.appendChild(leftSide);
              anchor.appendChild(rightSide);
            }
            else { // is featured
              anchor.appendChild( categoryLabel );
              anchor.appendChild( title );
              anchor.appendChild( publishDate );
              anchor.appendChild( imageWrapper );
            }

            // Append anchor to list container
            list.appendChild(anchor);
            // And append the prepared list to document fragment
            fragment.appendChild(list);
          }

          return fragment;
        },
        cseResult: function(data) {
          var fragment = document.createDocumentFragment(),
              i = 0, max_i = data.items.length;
          var parseTitle = function(titleText) {
            if( titleText.indexOf(' | ') > 0 ) {
              return titleText.split(' | ')[0];
            }
            else if( titleText.indexOf(' ...') > 0 ) {
              return titleText.substring(0, titleText.indexOf(' ...') );
            }
            return titleText;
          };

          for( i; i < max_i; i += 1 ) {
            // Create list
            var list = document.createElement('li');
            list.className = "regular clearfix";
            list.appendChild( document.createElement('a') );
            // Create anchor
            var anchor = list.children[0];
            anchor.href = data.items[i].link.split('rimanews.com')[1];
            // anchor.className = 'col-xs-12';
            anchor.dataset.hash = ( anchor.getAttribute('href').split('/')[3] === 'read' ) ?
                                  anchor.getAttribute('href').split('/')[5] :
                                  anchor.getAttribute('href').split('/')[4];
            anchor.dataset.title = parseTitle(data.items[i].title);
            // Create news image
            var imageWrapper = document.createElement('div'),
                image = imageWrapper.appendChild( document.createElement('img') );
            imageWrapper.className = "image-box";
            imageWrapper.appendChild(image);
            imageWrapper.appendChild( createImageSpinner('small', 'block') );
            image.className = "b-lazy";
            image.src = "data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAABAAAAAJCAQAAACRI2S5AAAAEElEQVR42mNkIAAYRxWAAQAG9gAKqv6+AwAAAABJRU5ErkJggg==";
            image.dataset.src = ( typeof data.items[i].pagemap !== 'undefined' ) ? data.items[i].pagemap.cse_image[0].src : '';
            // Prepare rubric category label
            var categoryLabel = document.createElement('span');
            categoryLabel.className = "category-label";
            categoryLabel.innerHTML = anchor.getAttribute('href').split('/')[1];
            // Prepare news title
            var title = document.createElement('h3');
            title.innerHTML = parseTitle(data.items[i].title);
            // Prepare news publication date
            var publishDate = document.createElement('time'),
                rawDate = ( anchor.getAttribute('href').split('/')[3] === 'read' ) ?
                          anchor.getAttribute('href').split('/')[4] :
                          anchor.getAttribute('href').split('/')[3];
            publishDate.setAttribute( "datetime", moment( rawDate ).format('YYYY-MM-DD') );
            publishDate.innerHTML = ( document.documentElement.lang === 'id' ) ?
                                    moment( rawDate ).format('D MMMM YYYY') :
                                    moment( rawDate ).format('MMMM D, YYYY');

            var leftSide = document.createElement('div'),
                rightSide = document.createElement('div');
            // Prepare left side of list row
            leftSide.className = "left-hand pull-left";
            leftSide.appendChild( imageWrapper );
            leftSide.appendChild( categoryLabel );
            // Prepare right side of list row
            rightSide.className = "right-hand pull-left";
            rightSide.appendChild( title );
            rightSide.appendChild( publishDate );
            // Merge the left and right hand into new list element
            anchor.appendChild(leftSide);
            anchor.appendChild(rightSide);

            // Append anchor to list container
            list.appendChild(anchor);
            // And append the prepared list to document fragment
            fragment.appendChild(list);
          }

          return fragment;
        }
      },
      subscribe: null
    };

    /////////////////
    // Pages: Read //
    /////////////////
    _page = (function() {
      return {
        /**
         * [news description]
         * @type {[type]}
         */
        news: null,
        /**
         * [article description]
         * @type {[type]}
         */
        article: null,
        /**
         * [init description]
         * @return {[type]} [description]
         */
        init: function(callback) {
          // this.container.classList.remove( app.params.hiddenClass );

          // if( !( this.article instanceof Article ) ) {
          //   this.article = new Article( "323353", this.container );
          // }

          if( typeof callback === 'function' ) callback();
        },
        /**
         * [setActive description]
         * @param {Function} callback [description]
         */
        setActive: function(callback) {
          // app.topbar.breadcrumb.innerHTML = app.params.readHeader;
          // app.topbar.breadcrumb.className = '';
          // app.topbar.togglers[0].dataset.pageTarget = (app.isDesktop) ? 'landing' : app.activePage;
          // app.topbar.togglers[0].children[0].className = 'iconorima icon-arrow-back';
          // // app.topbar.togglers[0].parentNode.className = '';
          // app.topbar.setTogglerVisibility( 1, 'show' );
          // // app.topbar.searchForm.parentNode.className = app.params.hiddenClass;
          // app.topbar.setStyle( false, true );
          // app.activePage = 'read';

          // @deprecated
          // this.init();
          
          if( this.container.children.length === 0 ) {
            this.container.appendChild( document.createElement('article') ).className = 'clearfix';
          }

          this.container.classList.remove( app.params.hiddenClass );

          if( typeof callback === 'function' ) callback();
        },
        /**
         * [setInactive description]
         * @param {Function} callback [description]
         */
        setInactive: function(callback) {
          // @deprecated
          // this.news.subs[0].destroy(true, true);
          // this.news.super.destroy(true, true);
          // this.news = null;
          // this.container.className += ' ' + app.params.hiddenClass;
          // this.container.setAttribute('style', '');
          // this.container.removeChild( this.container.firstElementChild );
          
          if( this.article instanceof Article ) {
            this.article = this.article.destroy();
          }

          this.container.className += ' ' + app.params.hiddenClass;

          if( typeof callback === 'function' ) callback();
        },
        /**
         * [createNews description]
         * @param  {[type]}  id        [description]
         * @param  {Boolean} isSponsor [description]
         * @return {[type]}            [description]
         */
        createNews: function(id, isSponsor) {
          // @deprecated
          // this.news.createById(hash);
          // var preloader = this.news.createArticlePreloader(),
          //     context = this;
          
          var context = this;
          // @deprecated
          // this.container.appendChild( preloader );
          // this.news.createById( hash, function() {
          //   context.container.removeChild( context.container.lastElementChild );
          // });

          if( this.article instanceof Article ) {
            this.article = this.article.destroy();
          }

          this.article = new Article( id, isSponsor, this.container.children[0] );

          var fetch = window.setTimeout(function() {
            window.clearTimeout( fetch );
            context.article.fetchContent(function() {
              // history.replaceState( null, document.title, slide.dataset.uri );
              // document.getElementById('share-control').classList.remove( app.params.hiddenClass );
              blazy.load( context.article.container.getElementsByClassName('b-lazy') );
            });
          }, 500);
        },
        /**
         * [onNewsItemClick description]
         * @param  {[type]} anchor        [description]
         * @param  {[type]} returnAsEvent [description]
         * @return {[type]}               [description]
         */
        onNewsItemClick: function(anchor, returnAsEvent) {
          var context = this;
          var invokeRead = function() {
            // @deprecated
            // if( context.container.children.length === 0 ) {
            //   context.container.appendChild( document.createElement('article') ).className = 'clearfix';
            // }

            // if( !( context.news instanceof Slides ) ) context.news = new Slides('#read');
            // app.url.path = anchor.getAttribute('href');

            // if( app.activePage !== 'read' ) {
            //   context.setActive();
            // }

            if( app.isDesktop && app.activePage === 'search' ) {
              app.search.setInactive();
            }

            if( !( context.article instanceof Article) ) {
              var currentDocumentPath = app.url.path;
              console.log(currentDocumentPath);

              app.activePage = 'read';
              app.url.path = anchor.getAttribute('href');
              app.rhl.add( anchor.dataset.title );
              app.topbar.togglers[0].dataset.pageTarget = app.rhl.states[ app.rhl.activeIndex ].previousPage;
              app.topbar.togglers[0].setAttribute( 'href', currentDocumentPath );
              
              app.topbar.update();
            }
            else {
              app.url.path = anchor.getAttribute('href');
              app.rhl.replace( anchor.dataset.title );
            }
            
            if( context.container.classList.contains( app.params.hiddenClass ) ) {
              context.setActive();
            }

            
            // @deprecated
            // else app.topbar.setStyle(true, false);

            context.createNews(
              anchor.dataset.hash,
              ( anchor.parentNode.classList.contains('featured') ) ? true : false
            );

            // app.rhl.setNewHistory( 'push', app.url.path, anchor.getAttribute('href') );
          };

          if( returnAsEvent ) {
            return function(event) {
              event.preventDefault();
              event.stopPropagation();

              invokeRead();
            };
          }
          else {
            return invokeRead;
          }
        },
      };
    }());
    app.read = new Page( document.getElementById( app.params.readContainerId ), _page );
    
    ////////////////////
    // Pages: Landing //
    ////////////////////
    var appendTimestamp;
    _page = (function() {
      return {
        /**
         * [swiper description]
         * @type {Swiper}
         */
        swiper: null,
        /**
         * [news description]
         * @type {Array|Swiper}
         */
        news: [],
        /**
         * [newsOffset description]
         * @type {Number}
         */
        newsOffset: 0,
        /**
         * [article description]
         * @type {Article}
         */
        article: null,
        /**
         * [init description]
         * @return {[type]} [description]
         */
        init: function() {
          var context = this,
              preloadedContents = this.container.querySelectorAll( '.' + app.params.articlePrefixClass + 'pre' ),
              preloadedContentsLength = preloadedContents.length,
              i, max_i;

          this.swiper = new Swiper(this.container, {
            // Properties
            direction: 'horizontal',
            spaceBetween: 0,
            resistance: true,
            resistanceRatio: 0,
            nested: true,
            simulateTouch: false,
            prevButton: ( app.isDesktop ) ? document.getElementById('landing-prev') : null,
            nextButton: ( app.isDesktop ) ? document.getElementById('landing-next') : null,
            keyboardControl: ( app.isDesktop ) ? true : false,
            // Events
            onSlideChangeEnd: function(instance) {
              context.onSwiperChange.bidirectional.end(context, instance);
            }
          });

          for( i = 0, max_i = preloadedContentsLength; i < max_i; i += 1 ) {
            this.news.push(
              new Swiper( preloadedContents[i], {
                // Properties
                direction: 'vertical',
                spaceBetween: 0,
                resistance: true,
                resistanceRatio: 0,
                simulateTouch: false,
                keyboardControl: ( app.isDesktop ) ? true : false,
                // Events
                onReachBeginning: function(instance) {
                  context.onNewsReach.beginning( context, instance, context.swiper.slides[ context.swiper.activeIndex ] );
                },
                onReachEnd: function(instance) {
                  context.onNewsReach.end( context, instance, context.swiper.slides[ context.swiper.activeIndex ] );
                },
                onTouchStart: function(instance, event) {
                  if( !app.isDesktop ) {
                    context.onNewsArticleScroll.touchStart(context, instance, event);
                  }
                  else return;
                },
                onTouchMove: function(instance, event) {
                  if( !app.isDesktop ) {
                    context.onNewsArticleScroll.touchMove(context, instance, event);
                  }
                  else return;
                }
              })
            );

            // Start appending title on news cover
            var fragment = document.createDocumentFragment(),
                slide = this.swiper.slides[i],
                cover = this.news[i].slides[0],
                title = cover.getElementsByClassName('cover-title')[0],
                mainTitle, subTitle;
            // Create cover title element and its properties
            if( typeof slide.dataset.solidCover !== 'undefined' ) {
              title.style.backgroundColor = slide.dataset.solidCover;
            }
            mainTitle = document.createElement('p');
            mainTitle.innerHTML = slide.dataset.title;
            mainTitle.className = 'title-heading';
            if( typeof slide.dataset.subTitle === 'undefined' ) {
              mainTitle.className += ' no-paragraph-margin';
              fragment.appendChild(mainTitle);
            }
            else {
              subTitle = document.createElement('p');
              subTitle.className = 'subtitle-heading no-paragraph-margin';
              subTitle.innerHTML = slide.dataset.subTitle;
              fragment.appendChild( mainTitle );
              fragment.appendChild( subTitle );
            }
            // Append the title
            title.appendChild( fragment );
          }

          this.newsOffset = this.news.length;

          if( app.isDesktop ) {
            this.swiper.wrapper[0].addEventListener('click', context.onReadButtonClick, false);
          }

          this.container.querySelector('#article-jumper').addEventListener('click', context.onJumperClick, false);
        },
        /**
         * [destroy description]
         * @param  {Function} callback [description]
         * @return {[type]}            [description]
         */
        destroy: function(callback) {
          if( typeof callback === 'function' ) callback();
        },
        /**
         * [setActive description]
         * @param {Function} callback [description]
         */
        setActive: function(callback) {
          if( !(this.swiper instanceof Swiper) && this.news.length < 1 ) this.init();
          if( this.article instanceof Article &&
              app.topbar.togglers[1].classList.contains( app.params.hiddenClass ) )
          {
            app.topbar.setTogglerVisibility( 1, 'show' );
          }

          if( typeof callback === 'function' ) callback();
        },
        /**
         * [setInactive description]
         * @param {Function} callback [description]
         */
        setInactive: function(callback) {
          if( typeof callback === 'function' ) callback();
        },
        /**
         * [append description]
         * @param  {[type]} category            [description]
         * @param  {[type]} orderBy             [description]
         * @param  {[type]} number              [description]
         * @param  {[type]} calledFromPreloader [description]
         * @return {[type]}                     [description]
         */
        append: function(category, orderBy, number, calledFromPreloader) {
          // calledFromPreloader = ( typeof calledFromPreloader !== 'boolean' ) ? false : calledFromPreloader;

          var context = this,
              apiTarget = 'getnewsdetail/' + category + '/' + orderBy + '/' + this.newsOffset + '/' + number,
              request;

          request = APIRequest('GET', apiTarget, {
            onloadstart: function() {
              // _this.isOnRequest = true;
            },
            onload: function(response) {
              // var response = request.responseData;
              // var slidesLength = _this.subs.length;
              // var duplicate = false;
              // var i = 0;

              var newsTotal = context.news.length,
                  duplicate = false,
                  i, max_i;

              if( newsTotal > 0 ) { //newsOffset
                for( i = 0, max_i = number; i < max_i; i += 1 ) {
                  // @deprecated
                  // if( _this.hashes[slidesLength - 1] == response[i].hash ) {
                  //   duplicate = !duplicate;
                  //   _this.offset += (i + 1);
                  //   break;
                  // }
                  // else {
                  //   _this.offset++;
                  //   _this.hashes.push( response[i].hash );
                  //   continue;
                  // }
                  
                  if( context.swiper.slides[newsTotal - 1].dataset.hash === response[i].hash ) {
                    // If news from response was previously added to swiper,
                    // then add news offset by (i + 1), set duplicate flag to true,
                    // and break the loop.
                    duplicate = true;
                    context.newsOffset += (i + 1);
                    break;
                  }
                  else {
                    // Else if news from response was not in swiper,
                    // then push news from response to instance and add news offset by 1
                    context.newsOffset += 1;
                    continue;
                  }
                }
              }
              // @deprecated
              // else {
              //   for( i=0; i<number; i++ ) {
              //     _this.hashes.push( response[i].hash );
              //   }
              // }

              if( !duplicate ) {
                // @deprecated
                // _this.prepare( response, true, function(preparedMarkup) {
                //   _this.render( preparedMarkup, function() {
                //     console.log(slidesLength);
                //     if( calledFromPreloader ) {
                //       _this.super.slideNext(true, 0);
                //       _this.super.removeSlide( _this.super.previousIndex );
                //     }
                //     else
                //       _this.super.removeSlide( slidesLength );

                //     if( slidesLength > 0 ) {
                //       _this.initContentOnScroll(slidesLength - 1);
                //       _this.initContentOnReachPosition(slidesLength - 1);
                //     }
                //     else {
                //       _this.initContentOnScroll();
                //       _this.initContentOnReachPosition();
                //     }

                //     _this.isOnRequest = false;
                //     // blazy.revalidate();
                //   });
                // });
                
                appendTimestamp = moment().unix();
                context.createSlides(response, function(slides) {
                  // Start to render/append new DOM into page
                  context.swiper.appendSlide( slides );

                  // Add new instance for each slides into news object
                  for( i = 0, max_i = slides.length - 1; i < max_i; i += 1 ) {
                    context.news.push(
                      new Swiper( '#' + response[i].hash + '__' + String( appendTimestamp ), {
                        // Properties
                        direction: 'vertical',
                        spaceBetween: 0,
                        resistance: true,
                        resistanceRatio: 0,
                        simulateTouch: false,
                        keyboardControl: ( app.isDesktop ) ? true : false,
                        // Events
                        onReachBeginning: function(instance) {
                          context.onNewsReach.beginning( context, instance, context.swiper.slides[ context.swiper.activeIndex ] );
                        },
                        onReachEnd: function(instance) {
                          context.onNewsReach.end( context, instance, context.swiper.slides[ context.swiper.activeIndex ] );
                        },
                        onTouchStart: function(instance, event) {
                          if( !app.isDesktop ) {
                            context.onNewsArticleScroll.touchStart(context, instance, event);
                          }
                          else return;
                        },
                        onTouchMove: function(instance, event) {
                          if( !app.isDesktop ) {
                            context.onNewsArticleScroll.touchMove(context, instance, event);
                          }
                          else return;
                        }
                      })
                    );
                  }

                  if( calledFromPreloader ) {
                    context.swiper.slideNext(true, 0);
                    context.swiper.removeSlide( context.swiper.previousIndex );
                    blazy.load( context.news[ context.swiper.activeIndex ].container[0].getElementsByClassName('b-lazy')[0] );
                  }
                  else context.swiper.removeSlide( newsTotal );

                  // if( newsTotal > 0 ) {
                  //   context.initContentOnScroll( newsTotal - 1 );
                  //   context.initContentOnReachPosition( newsTotal - 1 );
                  // }
                  // else {
                  //   context.initContentOnScroll();
                  //   context.initContentOnReachPosition();
                  // }
                });
              }
              else {
                // @deprecated
                // if( calledFromPreloader ) context.append(category, orderBy, number, true);
                // else context.append(category, orderBy, number);
                context.append( category, orderBy, number, (calledFromPreloader) ? true : false );
              }
            }
          });
          
          request.send(null);
          request = null;
        },
        /**
         * [createSlides description]
         * @param  {[type]}   data     [description]
         * @param  {Function} callback [description]
         * @return {[type]}            [description]
         */
        createSlides: function(data, callback) {
          var containerClassName = app.params.articlePrefixClass + String( appendTimestamp ),
              collections = [],
              i, max_i;

          for( i = 0, max_i = data.length; i < max_i; i += 1 ) {
            // Create outer slide container
            var fragment = document.createDocumentFragment(),
                slide = document.createElement('div');
            slide.className = 'swiper-slide';
            slide.dataset.hash = data[i].hash;
            slide.dataset.title = data[i].cover.title;
            slide.dataset.bgType = data[i].cover.background.type;
            slide.dataset.uri = data[i].news.href.substring(19);
            slide.appendChild( document.createElement('div') );
            // Create inner slide container
            var container = slide.children[0];
            container.className = 'swiper-container ' + app.params.newsContentClass + ' ' + containerClassName;
            container.id = data[i].hash + '__' + appendTimestamp;
            container.appendChild( document.createElement('div') );
            // Assign dynamic element of slide wrapper to a new variable
            // for later DOM processing
            var wrapper = container.children[0];
            wrapper.className = 'swiper-wrapper news-item-wrapper';
            // Append the cover and article now
            wrapper.appendChild( this.fragments.landing.cover( data[i] ) );
            wrapper.appendChild( this.fragments.landing.article() );

            fragment.appendChild( slide );

            collections.push( fragment );
          }

          // Create preloader slide
          collections.push( this.fragments.landing.preloader() );

          if( typeof callback === 'function' ) callback( collections );
        },
        /**
         * [onSwiperChange description]
         * @type {Object}
         */
        onSwiperChange: {
          bidirectional: {
            start: function(context, instance) {
              (function(ctx, inst) {
                // 
              })(context, instance);
            },
            end: function(context, instance) {
              (function(ctx, inst) {
                // Start lazy load news cover image on current slide
                // and remove the news cover image on previous slide
                var coverImage = ( inst.activeIndex < ctx.news.length ) ?
                      ctx.news[inst.activeIndex].container[0].getElementsByClassName('b-lazy')[0] :
                      null;
                var prevCoverImage = ( inst.previousIndex < ctx.news.length ) ?
                      ctx.news[inst.previousIndex].container[0].getElementsByClassName('b-lazy')[0] :
                      null;
                if( ( coverImage !== null && typeof coverImage !== 'undefined' ) &&
                    !coverImage.classList.contains('b-loaded') )
                {
                  if( !coverImage.parentNode.querySelector('.rima-lazy-spinner') ) {
                    // create spinner if spinner is not exist
                    coverImage.parentNode.appendChild( createImageSpinner('large', 'block') );
                  }
                  blazy.load( coverImage );
                }
                if( ( prevCoverImage !== null && typeof prevCoverImage !== 'undefined' ) &&
                    prevCoverImage.classList.contains('b-loaded') )
                {
                  prevCoverImage.classList.remove('b-loaded');
                  prevCoverImage.dataset.src = prevCoverImage.src;
                  prevCoverImage.src = "data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAABAAAAAJCAQAAACRI2S5AAAAEElEQVR42mNkIAAYRxWAAQAG9gAKqv6+AwAAAABJRU5ErkJggg==";
                }

                // Start appending swiper slide
                // if active slide index greater than previous slide index
                if( inst.activeIndex > inst.previousIndex &&
                    inst.activeIndex >= ctx.news.length - 2 )
                {
                  // If swiper is at the end of slides, set last param to true
                  // that indicates the append was called from slide preloader
                  ctx.append(0, 0, 5, (inst.isEnd) ? true : false );
                }
                else return;
              }(context, instance));
            }
          },
          next: {
            start: function(context, instance) {
              (function(ctx, inst) {
                // 
              })(context, instance);
            },
            end: function(context, instance) {
              (function(ctx, inst) {
                // 
              })(context, instance);
            }
          },
          prev: {
            start: function(context, instance) {
              (function(ctx, inst) {
                // 
              })(context, instance);
            },
            end: function(context, instance) {
              (function(ctx, inst) {
                // 
              })(context, instance);
            }
          }
        },
        /**
         * [onNewsChange description]
         * @type {Object}
         */
        // onNewsChange: {
        //   start: function() {},
        //   end: function() {}
        // },
        /**
         * [onNewsReach description]
         * @type {Object}
         */
        onNewsReach: {
          beginning: function(context, newsInstance, swiperSlide) {
            (function(ctx, inst, slide) {
              // @deprecated
              // app.url.hash = '#' + slide.dataset.hash;

              if( ctx.article instanceof Article ) {
                app.topbar.setStyle( true, false );
                app.url.path = '/';
                document.title = 'Rimanews.com';
                app.topbar.setTogglerVisibility( 1, 'hide' );
                history.replaceState( null, document.title, app.url.path );
                
                ctx.swiper.unlockSwipes();
                if( app.isDesktop ) {
                  ctx.swiper.enableKeyboardControl();
                  ctx.container.querySelector('.swiper-navigator').classList.remove('disabled');
                }
                else {
                  ctx.container.querySelector('.news-slide-hint').classList.remove( app.params.hiddenClass );
                }

                ctx.container.querySelector('#article-jumper').className = app.params.hiddenClass;

                // Remove all instance properties and assign with null
                ctx.article = ctx.article.destroy();
              }
            })(context, newsInstance, swiperSlide);
          },
          end: function(context, newsInstance, swiperSlide) {
            (function(ctx, inst, slide) {
              if( ctx.article instanceof Article ) return;
              
              var id = slide.dataset.hash,
                  articleContainer = inst.slides[1].children[0];

              ctx.swiper.lockSwipes();
              if( app.isDesktop ) {
                ctx.swiper.disableKeyboardControl();
                ctx.container.querySelector('.swiper-navigator').className += " disabled";
              }
              else {
                ctx.container.querySelector('.news-slide-hint').className += ' '+ app.params.hiddenClass;
              }
              ctx.article = new Article( id, false, articleContainer );
              app.topbar.setStyle( false, true );
              app.activeNews = slide.dataset.title;
              app.url.path = slide.dataset.uri;

              var fetch = window.setTimeout(function() {
                window.clearTimeout( fetch );
                ctx.article.fetchContent(function() {
                  document.title = 'Rimanews.com | ' + slide.dataset.title;
                  history.replaceState( null, document.title, slide.dataset.uri );
                  app.topbar.setTogglerVisibility( 1, 'show' );
                  ctx.container.querySelector('#article-jumper').className = "";
                  blazy.load( articleContainer.getElementsByClassName('b-lazy') );
                });
              }, 500);
            })(context, newsInstance, swiperSlide);
          }
        },
        /**
         * [onNewsArticleScroll description]
         * @type {Object}
         */
        onNewsArticleScroll: {
          touchStart: function(context, instance, event) {
            (function(ctx, inst, ev) {
              if( !inst.isEnd ) return;

              ctx.article.scrollPos = inst.slides[1].scrollTop;
              ctx.article.touchStartPos = ev.targetTouches[0].pageY;
            })(context, instance, event);
          },
          touchMove: function(context, instance, event) {
            (function(ctx, inst, ev) {
              var indexOnTouch = inst.activeIndex,
                  scrollArea = inst.slides;

              scrollArea.on('touchmove', function(_event) {
                if( !inst.isEnd ) return;
                if( indexOnTouch !== inst.activeIndex ) indexOnTouch = inst.activeIndex;
                ctx.article.touchCurrentPos = _event.targetTouches[0].pageY;

                var touchDiff = ctx.article.touchCurrentPos - ctx.article.touchStartPos,
                    activeContent = scrollArea[indexOnTouch];
                var onlyScrolling = (
                  // Allow scroll only when activeContent is scrollable
                  ( activeContent.scrollHeight > activeContent.offsetHeight ) &&
                  (
                    // Allow when scrolling is start from top edge to bottom, or
                    ( touchDiff < 0 && ctx.article.scrollPos === 0 ) ||
                    // Allow when scrolling is start from bottom edge to scroll top, or
                    ( touchDiff > 0 && ctx.article.scrollPos === ( activeContent.scrollHeight - activeContent.offsetHeight ) ) ||
                    // Allow when scrolling is start from the middle
                    ( ctx.article.scrollPos > 0 && ctx.article.scrollPos < ( activeContent.scrollHeight - activeContent.offsetHeight ) )
                  )
                );

                if( onlyScrolling ) _event.stopPropagation();
              });
            })(context, instance, event);
          }
        },
        /**
         * [onReadButtonClick description]
         * @return {[type]} [description]
         */
        onReadButtonClick: function() {
          event.stopPropagation();

          var context = app.landing,
              button = closestParent(event.target, 'button', this);

          if( button && button.classList.contains('read-this-button') ) {
            context.news[ context.swiper.activeIndex ].slideNext();
          }
          else return;
        },
        /**
         * [onJumperClick description]
         * @return {[type]} [description]
         */
        onJumperClick: function() {
          event.stopPropagation();

          var context = app.landing,
              button = closestParent(event.target, 'button', this);

          if( button && !button.classList.contains( app.params.hiddenClass ) ) {
            context.news[ context.swiper.activeIndex ].slideTo(0);
            this.className = app.params.hiddenClass;
          }
        }
      };
    }());
    app.landing = new Page( document.getElementById( app.params.landingContainerId ), _page );

    ////////////////////
    // Pages: Rubrics //
    ////////////////////
    var _spinnerInitTranslateY = Math.ceil( (app.containerHeight / 2) - (48 / 2) );
    _page = (function() {
      return {
        // Init later properties:
        // categories: object,
        // dimension: { padding: number, category: number, listing: number },
        // hash: array,
        // listings: object,
        // loadMoreButton: node,
        activeCategory: 0,
        activeListing: 0,
        spinner: {
          append: function() {
            if( document.getElementById('spinner') ) return;

            var context = app.rubrics;

            document.body.appendChild( context.fragments.rubrics.spinner.cloneNode(true) );
          },
          remove: function() {
            if( !document.getElementById('spinner') ) return;

            document.body.removeChild( document.getElementById('spinner') );
          },
          setState: function(state) {
            var context = this,
                spinner = document.getElementById('spinner'),
                iconWrapper = spinner.children[1];

            if( state === 'hide' ) {
              iconWrapper.addEventListener(
                applyPrefixedListener('TransitionEnd'),
                context.onHideTransitionEnd,
                false
              );

              window.getComputedStyle( iconWrapper ).getPropertyValue("transform");
              iconWrapper.style.transform = "translateY(-"+ String( _spinnerInitTranslateY ) +"px) scale(0)";
            }
            else if( state === 'show' ) {
              spinner.className = '';
              spinner.style.visibility = 'visible';
              window.getComputedStyle( iconWrapper ).getPropertyValue("transform");
              iconWrapper.style.transform = "translateY(0px) scale(1)";
            }
            else return;
          },
          onHideTransitionEnd: function(event) {
            var context = app.rubrics.spinner,
                target = event.target,
                parent = target.parentNode;

            parent.className = app.params.hiddenClass;
            parent.style.visibility = 'hidden';

            target.removeEventListener(
              applyPrefixedListener('TransitionEnd'),
              context.onHideTransitionEnd,
              false
            );
          },
        },
        init: function(callback) {
          var context = this,
              i, max_i;

          this.categories = {};
          this.categories.container = this.container.children[0];
          this.categories.items = this.categories.container.querySelectorAll('a');

          this.listings = {};
          this.listings.container = this.container.children[1];
          this.listings.togglerWrapper = this.listings.container.children[0];
          this.listings.contentWrapper = this.listings.container.children[1];
          this.listings.items = this.listings.contentWrapper.querySelectorAll('ul');

          this.loadMoreButton = this.listings.contentWrapper.lastElementChild.lastElementChild;

          this.dimension = {
            padding : parseInt( window.getComputedStyle( context.container, null ).getPropertyValue('padding-top').split('px')[0] ),
            category: this.categories.container.clientHeight,
            listings : this.listings.container.clientHeight
          };

          i = 0; max_i = this.listings.items.length;
          this.hash = [];
          for( i; i < max_i; i += 1 ) {
            var anchors = this.listings.contentWrapper.children[i].querySelectorAll('a'),
                j = 0, max_j = anchors.length;

            this.hash[i] = [];

            for( j; j < max_j; j += 1 ) {
              this.hash[i].push( anchors[j].dataset.hash );
            }
          }

          // this.container.addEventListener('scroll', context.onListingScroll, false);
          this.categories.container.addEventListener('click', context.onCategoryClick, false);
          this.listings.togglerWrapper.addEventListener('click', context.onListingsTogglerClick, false);
          this.listings.contentWrapper.addEventListener('click', context.onNewsItemClick, false);
          this.loadMoreButton.addEventListener('click', context.onLoadMoreButtonClick, false);

          if( typeof callback === 'function' ) callback();
        },
        destroy: function(callback) {
          var context = this,
              i, max_i;

          // this.container.removeEventListener('scroll', context.onListingScroll, false);
          this.categories.container.removeEventListener('click', context.onCategoryClick, false);
          this.listings.togglerWrapper.removeEventListener('click', context.onListingsTogglerClick, false);
          this.listings.contentWrapper.removeEventListener('click', context.onNewsItemClick, false);
          this.loadMoreButton.removeEventListener('click', context.onLoadMoreButtonClick, false);

          this.categories.container.removeChild( this.categories.container.children[0] );
          i = 0; max_i = 2;
          for( i; i < max_i; i += 1 ) {
            this.listings.contentWrapper.removeChild( this.listings.items[i] );
          }
          this.spinner.remove();

          delete this.categories;
          delete this.listings;
          delete this.hash;
          delete this.dimension;

          if( typeof callback === 'function' ) callback();
        },
        setActive: function(callback) {
          var context = this,
              wasInited = ( typeof this.categories !== 'undefined' && typeof this.listings !== 'undefined' ),
              isHiddenOnMobile = ( context.container.classList.contains('hidden-xs') &&
                                   context.container.classList.contains('hidden-sm') &&
                                   context.container.classList.contains('hidden-md') );

          if( !wasInited ) {
            var apiTarget = 'getfirstinit/0/0/9';
            var request = APIRequest('GET', apiTarget, {
              onloadstart: function() {
                context.spinner.append();
                context.spinner.setState('show');
                context.container.style.visibility = 'hidden';
                if( isHiddenOnMobile ) {
                  context.container.classList.remove('hidden-xs', 'hidden-sm', 'hidden-md');
                }
              },
              onload: function(response) {
                context.container.style.visibility = 'visible';

                context.render(response, function() {
                  context.init(function() {
                    context.spinner.setState('hide');
                    blazy.load( context.container.querySelectorAll("img.b-lazy") );

                    if( typeof callback === 'function' ) callback();
                  });
                });
              }
            });

            request.send(null);
            request = null;
          }
          else { // Rubrics page has been created
            if( isHiddenOnMobile ) {
              context.container.classList.remove('hidden-xs', 'hidden-sm', 'hidden-md');
            }

            if( callback && typeof callback === 'function' ) callback();
          }
        },
        setInactive: function(callback) {
          // @obsolete
          // this.container.className += ' '+ app.params.hiddenClass;
          this.container.className += ' hidden-xs hidden-sm hidden-md';

          if( typeof callback === 'function' ) callback();
        },
        render: function(data, callback) {
          var _categories = this.fragments.rubrics.category( data.rubrics ),
              _listings = this.fragments.rubrics.listings( data.listings );

          this.container.children[0].appendChild( _categories.htmlFragment );
          this.container.children[1].children[1].insertBefore( _listings.htmlFragment, document.getElementById('loadmore') );

          if( typeof callback === 'function' ) callback();
        },
        append: function(callback) {
          var context = this,
              // orderBy = (this.activeListing === 0) ? 1 : 0,
              orderBy = this.activeListing,
              request1, apiTarget1;
          var fetchAndAppendNews = function(_apiTarget) {
            return APIRequest('GET', _apiTarget, {
              onload: function(response) {
                for( var section in response ) {
                  if( section === 'listings' ) continue;
                  else if( section === 'sponsor' ) {
                    response.listings.splice( 1, 0, response.sponsor[0] );
                    response.listings[1].featured = true;
                    response.listings[1].rubric.main = "Sponsor";
                    response = response.listings;
                  }
                }

                var fragment = context.fragments.rubrics.newsItems(response),
                    anchors = fragment.querySelectorAll('a'),
                    i = 0, max_i = anchors.length;
                
                for( i; i < max_i; i += 1 ) {
                  context.hash[ context.activeListing ].push( anchors[i].dataset.hash );
                }

                context.listings.items[ context.activeListing ].appendChild( fragment );
                context.dimension.listings = context.listings.container.clientHeight;

                if( callback && typeof callback === 'function' ) callback();
              }
            });
          };

          if( this.activeListing === 0 ) { // active listing is newest listing
            apiTarget1 = 'getneighbors/' + this.hash[this.activeListing][0] + '/after/0/50';
            // Check offset first
            request1 = APIRequest('GET', apiTarget1, {
              onload: function(response) {
                var appendOffset = context.hash[context.activeListing].length + response.length,
                    request2, apiTarget2;
                // Fetch news by offset
                apiTarget2 = 'getnews/'+ context.activeCategory +'/'+ orderBy +'/'+ appendOffset +'/9';
                request2 = fetchAndAppendNews(apiTarget2);
                request2.send(null);
                request2 = null;
              }
            });
          }
          else { // active listing is popular listing
            apiTarget1 = 'getnews/'+ context.activeCategory +'/'+ orderBy +'/'+ context.hash[context.activeListing].length +'/9';
            // Fetch news without checking the offset
            request1 = fetchAndAppendNews(apiTarget1);
          }

          request1.send(null);
          request1 = null;
        },
        onCategoryClick: function() {
          event.preventDefault();
          event.stopPropagation();

          var anchor = closestParent(event.target, 'a', this),
              context = app.rubrics;

          if( anchor && context.activeCategory !== parseInt( anchor.dataset.rubricsId ) ) {
            app.url.path = anchor.getAttribute('href');
            // @todo: fix history modifier
            // app.rhl.setNewHistory('replace');

            context.loadMoreButton.disabled = true;
            context.loadMoreButton.className = 'no-border';
            context.listings.togglerWrapper.style.visibility = 'hidden';
            context.activeCategory = parseInt( anchor.dataset.rubricsId );
            context.listingsHeight = 0;
            
            var i = 0, max_i = 2;
            for( i; i < max_i; i += 1 ) {
              var ul = context.listings.items[i];

              if( !ul.classList.contains( app.params.hiddenClass ) ) {
                ul.className += ' '+ app.params.hiddenClass;
              }
              while( ul.lastChild ) ul.removeChild( ul.lastChild );
              context.hash[i] = [];
            }

            // Append news on newest listing first
            context.activeListing = 0;
            context.append(function() {
              // Now append news on popular listing
              context.activeListing = 1;
              context.append(function() {
                // Reset all UI and property
                var buttons = context.listings.togglerWrapper.querySelectorAll('button');
                if( buttons[1].classList.contains('active') ) {
                  buttons[1].classList.remove('active');
                  buttons[0].className += ' active';
                }
                context.activeListing = 0;
                context.listings.items[0].classList.remove( app.params.hiddenClass );
                context.dimension.listings = context.listings.container.clientHeight;
                context.listings.togglerWrapper.style.visibility = 'visible';
                context.loadMoreButton.disabled = false;
                context.loadMoreButton.className = '';

                blazy.load( context.container.querySelectorAll("img.b-lazy") );
              });
            });
          }
          else return;
        },
        onNewsItemClick: function() {
          var anchor = closestParent(event.target, 'a', this);
          if( anchor ) {
            if( anchor.target === '_blank' ) return;
            else {
              event.preventDefault();
              event.stopPropagation();

              app.read.onNewsItemClick(anchor)();
            }
          }
          else return;
        },
        onListingsTogglerClick: function() {
          event.stopPropagation();

          var target = event.target,
              context = app.rubrics;

          if( target.tagName.toLowerCase() === 'button' && !target.classList.contains('active') ) {
            var clickedIndex = parseInt(target.dataset.targetIndex),
                oppositeIndex = (clickedIndex === 1) ? 0 : 1;

            target.className += ' active';
            target.parentNode.querySelectorAll('button')[oppositeIndex].classList.remove('active');
            context.listings.items[clickedIndex].classList.remove('hidden');
            context.listings.items[oppositeIndex].classList.add('hidden');
            context.activeListing = clickedIndex;
            context.dimension.listings = context.listings.container.clientHeight;

            blazy.load( context.listings.items[clickedIndex].querySelectorAll("img.b-lazy") );
          }
          else return;
        },
        // onListingScroll: function() {
        //   var context = app.rubrics,
        //       totalHeight = context.dimension.padding + context.dimension.category + context.dimension.listings;

        //   if( this.scrollTop + app.containerHeight === totalHeight ) {
        //     context.loadMoreButton.disabled = true;
        //     context.append(function() {
        //       context.loadMoreButton.disabled = false;
        //       blazy.revalidate();
        //     });
        //   }
        // },
        onLoadMoreButtonClick: function() {
          var context = app.rubrics,
              button = this;
          button.disabled = true;
          context.append(function() {
            button.disabled = false;
            blazy.load( context.listings.items[ context.activeListing ].querySelectorAll("img.b-lazy") );
          });
        }
      };
    }());
    app.rubrics = new Page( document.getElementById( app.params.rubricsContainerId ), _page );

    ///////////////////
    // Pages: Search //
    ///////////////////
    var restrictedKey = [ 'alt', 'meta', 'control', 'shift', 'backspace', 'capslock', 'escape', 'f1', 'f2', 'f3', 'f4', 'f5', 'f6', 'f7', 'f8', 'f9', 'f10', 'f11', 'f12', 'arrowleft', 'arrowup', 'arrowright', 'arrowdown' ];
    _page = (function() {
      return {
        // Init later properties:
        // cse: object
        // listing: object
        // topbar: node
        // topics: object
        spinner: {
          append: function(target) {
            var context = app.search;
            target.appendChild( context.fragments.search.spinner.cloneNode(true) );
          },
          remove: function(target) {
            target.removeChild( target.lastElementChild );
          }
        },
        init: function(callback) {
          var context = this;

          this.topbar = this.container.children[0];

          this.topics = {};
          this.topics.container = this.container.children[1].children[0];
          this.topics.overlay = this.container.children[1].children[1];
          this.topics.items = this.topics.container.children;

          this.listing = {};
          this.listing.container = this.container.children[2].children[0];
          this.listing.items = null;

          this.cse = {};
          this.cse.container = this.container.children[3].querySelector('#suggestion-listing');
          this.cse.counter = this.cse.container.previousElementSibling.querySelector('#result-count');
          this.cse.loadMoreButton = this.cse.container.nextElementSibling.querySelector('button');
          this.cse.timer = null;

          this.topbar.addEventListener('click', context.onListingBackButtonClick, false);
          this.topics.container.addEventListener('click', context.onTopicClick, false);
          this.listing.container.addEventListener('click', context.onNewsItemClick, false);
          this.cse.container.addEventListener('click', context.onNewsItemClick, false);
          app.topbar.searchForm.addEventListener('submit', context.onSearchQuerySubmit, false);
          app.topbar.searchForm.children[0].addEventListener('click', context.onSearchBackButtonClick, false);
          this.cse.loadMoreButton.addEventListener('click', context.onLoadMoreButtonClick, false);

          if( typeof callback === 'function' ) callback();
        },
        destroy: function(callback) {
          var context = this;

          this.topbar.removeEventListener('click', context.onBackButtonClick, false);
          this.topics.container.removeEventListener('click', context.onTopicClick, false);
          this.listing.container.removeEventListener('click', context.onNewsItemClick, false);

          while( this.topics.container.lastElementChild ) {
            this.topics.container.removeChild( this.topics.container.lastElementChild );
          }

          delete this.topbar;
          delete this.topics;
          delete this.listing;

          if( typeof callback === 'function' ) callback();
        },
        setActive: function(callback) {
          var context = this,
              wasInited = ( typeof this.topics !== 'undefined' );

          console.log(wasInited);

          if( app.isDesktop ) {
            document.getElementById('app-desktop-overlay').className = '';
          }

          if( !wasInited ) {
            var apiTarget = 'gethottopics/0/0/10';
            var request = APIRequest('GET', apiTarget, {
              onloadstart: function() {
                // append spinner
                context.spinner.append( context.container.children[1] );
                // slide the container in
                context.container.addEventListener(
                  applyPrefixedListener('AnimationEnd'),
                  context.onSlideAnimationEnd,
                  false
                );
                context.container.classList.remove( app.params.hiddenClass );
                context.container.className += ' slideInRight';
              },
              onload: function(response) {
                context.render( response, 'topics', function() {
                  context.init(function() {
                    var i = 0, max_i = response.length,
                        j = 0, timing = 0;

                    context.spinner.remove( context.container.children[1] );

                    while( i < max_i ) {
                      context.topics.items[i].addEventListener(
                        applyPrefixedListener('AnimationEnd'),
                        context.onBounceAnimationEnd,
                        false
                      );

                      setTimeout(function() {
                        context.topics.items[j].style.visibility = 'visible';
                        context.topics.items[j].className += ' bounceIn';
                        j += 1;
                      }, timing);

                      timing += 55;
                      i += 1;
                    }

                    if( typeof callback === 'function' ) callback();
                  });
                });
              }
            });

            request.send(null);
            request = null;
          }
          else {
            context.container.addEventListener(
              applyPrefixedListener('AnimationEnd'),
              context.onSlideAnimationEnd,
              false
            );
            context.container.classList.remove( app.params.hiddenClass );
            context.container.className += ' slideInRight';

            if( typeof callback === 'function' ) callback();
          }
        },
        setInactive: function(callback) {
          var context = this;

          if( app.isDesktop ) {
            document.getElementById('app-desktop-overlay').className = app.params.hiddenClass;
          }

          this.container.addEventListener(
            applyPrefixedListener('AnimationEnd'),
            context.onSlideAnimationEnd,
            false
          );
          this.container.className += ' slideOutRight';
          if( !this.listing.container.parentNode.classList.contains( app.params.hiddenClass ) ) {
            this.setListingState( 'hide', true );
          }

          if( typeof callback === 'function' ) callback();
        },
        setListingState: function(state, properties) {
          var context = this,
              backButton = context.topbar.children[0],
              topicTitle = context.topbar.children[1];

          if( state === 'hide' ) {
            var immediateRemove = ( typeof properties === 'boolean' && properties === true );

            backButton.className += ' '+ app.params.hiddenClass;
            topicTitle.innerHTML = 'Topik Terpopuler';
            this.topics.overlay.className += ' '+ app.params.hiddenClass;

            if( immediateRemove ) {
              this.listing.container.parentNode.className += ' '+ app.params.hiddenClass;
            }
            else {
              this.listing.container.parentNode.addEventListener(
                applyPrefixedListener('AnimationEnd'),
                context.onSlideAnimationEnd,
                false
              );
              this.listing.container.parentNode.className += ' slideOutRight';
            }
            
            while( this.listing.container.lastChild ) {
              this.listing.container.removeChild( this.listing.container.lastChild );
            }
          }
          else if( state === 'show' ) {
            var topicName = properties;

            backButton.classList.remove( app.params.hiddenClass );
            topicTitle.innerText = (function() {
              if( topicName.length >= 25 ) return topicName.substring(0, 17) + '...';
              else return topicName;
            }());
            this.topics.overlay.classList.remove( app.params.hiddenClass );
            this.spinner.append( this.container.children[2] );

            this.listing.container.parentNode.addEventListener(
              applyPrefixedListener('AnimationEnd'),
              context.onSlideAnimationEnd,
              false
            );
            this.listing.container.parentNode.classList.remove( app.params.hiddenClass );
            this.listing.container.parentNode.className += ' slideInRight';
          }
          else return;
        },
        fetchGoogleCSEREST: function(apiKey, engineId, query, startFrom, callback) {
          query = encodeURIComponent(query);
          startFrom = ( typeof startFrom === 'undefined' || typeof startFrom !== 'number' ) ? 1 : startFrom;
          var context = this,
              apiTarget = 'https://www.googleapis.com/customsearch/v1?key='+ apiKey +'&cx='+ engineId +'&q='+ query +'&fields='+ encodeURIComponent( 'items(link,title,snippet,pagemap/cse_image),searchInformation/totalResults' ) +'&start='+ startFrom;
          var request = APIRequest('GET', apiTarget, {
            onloadstart: function() {},
            onload: function(response) {
              console.log(response);
              context.render( response, 'cseResult', function() {
                context.cse.counter.innerText = numberFormat( response.searchInformation.totalResults, '0', ',', '.' );
                context.cse.counter.parentNode.parentNode.classList.remove( app.params.hiddenClass );
                if( context.cse.container.classList.contains( app.params.hiddenClass ) ) {
                  context.cse.container.classList.remove( app.params.hiddenClass );
                }
                context.cse.container.style.visibility = 'visible';
                if( response.searchInformation.totalResults > 10 ) {
                  context.cse.loadMoreButton.dataset.resultStartFrom = startFrom + 10;
                  context.cse.loadMoreButton.parentNode.classList.remove( app.params.hiddenClass );
                }

                if( typeof callback === 'function' ) callback();
              });
            },
            onloadend: function() {
              app.topbar.searchForm.querySelector('input[type=search]').disabled = false;
            }
          });

          request.send(null);
          request = null;
        },
        render: function(data, renderType, callback) {
          if( renderType === 'topics' ) {
            this.container.children[1].children[0].appendChild( this.fragments.search.topics(data) );
          }
          else if( renderType === 'listing' ) {
            this.container.children[2].children[0].appendChild( this.fragments.search.listing(data) );
          }
          else if( renderType === 'cseResult' ) {
            this.container.children[3].children[1].appendChild( this.fragments.search.cseResult(data) );
          }
          else return;

          if( typeof callback === 'function' ) callback();
        },
        onTopicClick: function() {
          event.preventDefault();
          event.stopPropagation();

          var context = app.search,
              anchor = closestParent(event.target, 'a', this);

          if( anchor ) {
            var apiTarget = 'getnewsbytag/' + anchor.dataset.tagUri + '/0/10';
            var request = APIRequest('GET', apiTarget, {
              onloadstart: function() {
                context.setListingState('show', anchor.children[0].innerText);
              },
              onload: function(response) {
                context.render( response, 'listing', function() {
                  context.spinner.remove( context.container.children[2] );
                  blazy.load( context.listing.container.querySelectorAll('img.b-lazy') );
                });
              }
            });

            request.send(null);
            request = null;
          }
          else return;
        },
        onNewsItemClick: function() {
          event.preventDefault();
          event.stopPropagation();

          var anchor = closestParent(event.target, 'a', this);
          if( anchor ) {
            app.read.onNewsItemClick(anchor)();
            if( app.isDesktop ) {
              document.getElementById('app-desktop-overlay').className = app.params.hiddenClass;
            }
          }
          else return;
        },
        onListingBackButtonClick: function() {
          event.stopPropagation();

          var context = app.search,
              button = closestParent(event.target, 'button', this);
          if( button ) { context.setListingState('hide'); }
          else return;
        },
        onSearchBackButtonClick: function() {
          event.stopPropagation();
          event.preventDefault();

          var context = app.search,
              button = closestParent(event.target, 'a', app.topbar.searchForm),
              searchField = app.topbar.searchForm.children[0].querySelector('input[type="search"]'),
              resultWrapper = context.cse.container.parentNode;
          console.log(event.path);
          if( button ) {
            searchField.disabled = false;
            searchField.value = '';
            app.topbar.searchForm.children[0].dataset.lastQuery = '';
            app.topbar.searchForm.children[0].style.padding = '0 77px 0 15px';
            app.topbar.setTogglerVisibility( 2, 'show' );
            this.querySelector('#cse-back-trigger').className += ' '+ app.params.hiddenClass;

            if( !resultWrapper.classList.contains( app.params.hiddenClass ) ) {
              resultWrapper.className += ' '+ app.params.hiddenClass;
            }
            context.cse.counter.innerText = '';
            if( !context.cse.counter.parentNode.parentNode.classList.contains( app.params.hiddenClass ) ) {
              context.cse.counter.parentNode.parentNode.className += ' '+ app.params.hiddenClass;
            }
            if( context.cse.container.children.length > 0 ) {
              while( context.cse.container.lastElementChild ) {
                context.cse.container.removeChild( context.cse.container.lastElementChild );
              }
            }
            if( !context.cse.container.classList.contains( app.params.hiddenClass ) ) {
              context.cse.container.className = app.params.hiddenClass;
            }
            if( !context.cse.loadMoreButton.parentNode.classList.contains( app.params.hiddenClass ) ) {
              context.cse.loadMoreButton.parentNode.className = app.params.hiddenClass;
            }
          }
          else return;
        },
        onSearchQuerySubmit: function() {
          event.preventDefault();

          var context = app.search,
              searchField = event.target.children[1],
              resultWrapper = context.cse.container.parentNode;

          if( searchField.value ) {
            if( ( typeof event.target.dataset.lastQuery === 'undefined' ) ||
                (
                  typeof event.target.dataset.lastQuery !== 'undefined' &&
                  event.target.dataset.lastQuery !== encodeURIComponent( searchField.value )
                )
            ) {
              searchField.disabled = true;

              if( resultWrapper.classList.contains( app.params.hiddenClass ) ) {
                resultWrapper.classList.remove( app.params.hiddenClass );
              }
              if( !context.cse.counter.parentNode.parentNode.classList.contains( app.params.hiddenClass ) ) {
                context.cse.counter.parentNode.parentNode.className += ' '+ app.params.hiddenClass;
              }
              if( context.cse.container.children.length > 0 ) {
                while( context.cse.container.lastElementChild ) {
                  context.cse.container.removeChild( context.cse.container.lastElementChild );
                }
              }
              if( !context.cse.container.classList.contains( app.params.hiddenClass ) ) {
                context.cse.container.className = app.params.hiddenClass;
              }
              if( !context.cse.loadMoreButton.parentNode.classList.contains( app.params.hiddenClass ) ) {
                context.cse.loadMoreButton.parentNode.className = app.params.hiddenClass;
              }

              app.topbar.setTogglerVisibility( 2, 'hide' );
              app.topbar.searchForm.querySelector('#cse-back-trigger').classList.remove( app.params.hiddenClass );
              event.target.style.padding = '0 15px 0 77px';
              context.cse.container.style.visibility = 'hidden';
              context.spinner.append( resultWrapper );
              event.target.dataset.lastQuery = encodeURIComponent( searchField.value );
              context.cse.loadMoreButton.dataset.resultStartFrom = String(1);

              context.fetchGoogleCSEREST(
                'AIzaSyAxQl_YIc3QhwMK6TZ-CnDM_s6Dx6Gmr4s',
                '011803645223829055717:p75lnvlpuqo',
                searchField.value,
                1,
                function() {
                  context.spinner.remove( context.cse.container.parentNode );
                  blazy.load( context.cse.container.querySelectorAll('img.b-lazy') );
                }
              );
            }
            else {
              return;
            }
          }
          else return;
        },
        onSearchQueryTypedIn: function(event) {
          event.stopPropagation();

          // var context = app.search,
          //     resultWrapper = context.cse.container.parentNode;

          // clearTimeout( context.cse.timer );
          // console.log(event);
          // console.log(event.target.value);

          // if( restrictedKey.indexOf( event.key.toLowerCase() ) === -1 ) {
          //   // console.log( restrictedKey.indexOf( event.key.toLowerCase() ) );
            
          //   if(event.target.value) {
          //     if( resultWrapper.classList.contains( app.params.hiddenClass ) ) {
          //       resultWrapper.classList.remove( app.params.hiddenClass );
          //     }
          //     if( resultWrapper.children.length < 2 ) {
          //       context.spinner.append( resultWrapper );
          //     }

          //     /*context.cse.timer = setTimeout(function() {
          //       context.fetchGoogleCSEREST(
          //         'AIzaSyAxQl_YIc3QhwMK6TZ-CnDM_s6Dx6Gmr4s',
          //         '011803645223829055717:p75lnvlpuqo',
          //         event.target.value,
          //         context.cse.container.children.length + 1
          //       );
          //     }, 500);*/

          //     // if( trendingPicker.classList.contains('show') ) {
          //     //   classList(trendingPicker).toggle('hidden').remove('show');
          //     //   classList(suggestionPicker).remove('hidden');
          //     // }

          //     // typingTimer = setTimeout(function() {
          //     //   if(searchField.value.toLowerCase() == 'obama') {
          //     //     suggestionPicker.children[0].innerHTML = ohObama;
          //     //   }
          //     //   else {
          //     //     suggestionPicker.children[0].innerHTML = '';
          //     //   }
          //     // }, populateDelay);
          //   }
          //   else {
          //     // if( !trendingPicker.classList.contains('show') ) {
          //     //   classList(suggestionPicker).add('hidden');
          //     //   classList(trendingPicker).add('show').toggle('hidden');
          //     //   suggestionPicker.children[0].innerHTML = ''
          //     // }
          //   }
          // }
        },
        onLoadMoreButtonClick: function() {
          event.stopPropagation();

          var context = app.search,
              target = event.target;

          target.disabled = true;
          app.topbar.searchForm.querySelector('input[type="search"]').disabled = true;

          context.fetchGoogleCSEREST(
            'AIzaSyAxQl_YIc3QhwMK6TZ-CnDM_s6Dx6Gmr4s',
            '011803645223829055717:p75lnvlpuqo',
            app.topbar.searchForm.querySelector('input[type="search"]').value,
            parseInt( event.target.dataset.resultStartFrom ),
            function() {
              target.disabled = false;
              blazy.load( context.cse.container.querySelectorAll('img.b-lazy') );
            }
          );
        },
        onSlideAnimationEnd: function() {
          var context = app.search,
              target = event.target,
              animationName = event.animationName;

          target.classList.remove(animationName);
          if( animationName === 'slideOutRight' ) {
            target.className += ' '+ app.params.hiddenClass;
          }

          target.removeEventListener(
            applyPrefixedListener('AnimationEnd'),
            context.onSlideAnimationEnd,
            false
          );
        },
        onBounceAnimationEnd: function() {
          var context = app.search,
              target = event.target,
              animationName = event.animationName;

          target.classList.remove(animationName);

          target.removeEventListener(
            applyPrefixedListener('AnimationEnd'),
            context.onBounceAnimationEnd,
            false
          );
        },
      };
    }());
    app.search = new Page( document.getElementById( app.params.searchContainerId ), _page );
    
    ////////////////////
    // Pages: Sharing //
    ////////////////////
    var intents = function(platform, callback) {
      var text,
          url = app.url.origin + app.url.path;

      // console.log(app.url.path);

      if( platform === 'fb' ) {
        FB.ui({
          method: "share",
          href: url
        }, function(response){});

        return;
      }
      else {
        var href;

        url = encodeURIComponent(url);
        text = encodeURIComponent(app.activeNews);

        switch( platform ) {
          case 'twitter':
            href = 'https://twitter.com/intent/tweet?text=' + text + '&url=' + url + '&via=Rimanews';
          break;

          case 'gplus':
            href = 'https://plus.google.com/share?url=' + url;
          break;

          case 'line':
            href = 'line://msg/text/?' + text + '%0D%0A' + url;
          break;

          case 'whatsapp':
            href = 'whatsapp://send?text=' + text + '%0D%0A' + url;
          break;
        }

        callback( href );
        return;
      }
    };
    _page = (function() {
      return {
        // toggler: toggler,
        init: function() {
          // var context = this;

          // this.toggler.addEventListener('click', context.onTogglerClick, false);
        },
        setActive: function(callback) {
          this.container.classList.remove( app.params.hiddenClass );
          app.topbar.setStyle(true, false);
          app.topbar.setTogglerVisibility(0, 'hide');
          app.topbar.setTogglerVisibility(2, 'hide');
          app.topbar.breadcrumb.classList.remove('label-logo');
          app.topbar.breadcrumb.innerText = app.params.sharingHeader;
          app.topbar.togglers[1].querySelector('i').className += ' icon-close';
          app.topbar.togglers[1].querySelector('i').classList.remove('icon-share');

          if( typeof callback === 'function' ) callback();
        },
        setInactive: function(callback) {
          this.container.className += ' '+ app.params.hiddenClass;
          app.topbar.setStyle(false, true);
          app.topbar.setTogglerVisibility(0, 'show');
          app.topbar.setTogglerVisibility(2, 'show');
          app.topbar.breadcrumb.className = 'label-logo';
          // app.topbar.breadcrumb.innerText = app.params.sharingHeader;
          app.topbar.togglers[1].querySelector('i').className += ' icon-share';
          app.topbar.togglers[1].querySelector('i').classList.remove('icon-close');

          if( typeof callback === 'function' ) callback();
        },
        // setState: function(state, callback) {
        //   var context = app.sharing,
        //       breadcrumbText;

        //   if( state === 'hide' ) {
        //     breadcrumbText = (app.activePage === 'landing') ? app.params.landingHeader : app.params.readHeader;
        //     app.topbar.setStyle(false, true);
        //     app.topbar.toggleButtonVisibility(0, 'show');
        //     if( app.activePage === 'landing' || app.activePage === 'read' ) {
        //       app.topbar.toggleButtonVisibility(1, 'show');
        //     }
        //     app.topbar.breadcrumb.classList.remove('no-margin');
        //     app.topbar.breadcrumb.innerText = breadcrumbText;
        //     this.toggler.classList.remove('no-margin');
        //     this.toggler.children[0].children[0].className += ' icon-share';
        //     this.toggler.children[0].children[0].classList.remove('icon-close');
        //     this.container.className += ' '+ app.params.hiddenClass;
        //   }
        //   else if( state === 'show' ) {
        //     app.topbar.setStyle(true, false);
        //     app.topbar.toggleButtonVisibility(0, 'hide');
        //     if( app.activePage === 'landing' || app.activePage === 'read' ) {
        //       app.topbar.toggleButtonVisibility(1, 'hide');
        //     }
        //     app.topbar.breadcrumb.className += ' no-margin';
        //     app.topbar.breadcrumb.innerText = app.params.sharingHeader;
        //     this.toggler.className = 'no-margin';
        //     this.toggler.children[0].children[0].className += ' icon-close';
        //     this.toggler.children[0].children[0].classList.remove('icon-share');
        //     this.container.classList.remove( app.params.hiddenClass );
        //   }
        //   else return;

        //   if( typeof callback === 'function' ) callback();
        // },
        shareTo: function(platform, sourceNode) {
          event.preventDefault();

          var anchor;

          if( sourceNode.parentNode.children.length > 1 ) {
            anchor = sourceNode.previousElementSibling;
          }
          else anchor = null;

          intents(platform, function(intentURL) {
            if( anchor && platform !== 'fb' ) {
              anchor.href = intentURL;
              anchor.click();
              anchor.href = '#';
            }
          });
        },
        // onTogglerClick: function() {
        //   var context = app.sharing,
        //       button = closestParent(event.target, 'button', context.toggler);

        //   if( button ) {
        //     if( context.container.classList.contains( app.params.hiddenClass ) ) {
        //       context.setState('show');
        //     }
        //     else {
        //       context.setState('hide');
        //     }
        //   }
        // }
      };
    }());
    app.sharing = new Page( document.getElementById( app.params.sharingContainerId ), _page );

    // nullify the _page variable
    _page = null;
    
    ////////////////
    // Components //
    ////////////////
    app.subscribe = (function() {
      var _registerEmail = function(email, callback) {
        var apiTarget = 'subscribe/'+ encodeURIComponent(email) +'/1';
        console.log(email, apiTarget);
        var request = APIRequest('GET', apiTarget, {
          onload: function(response) {
            callback(response);
          }
        });

        request.send(null);
      };

      return {
        send: function() {
          var email = this.previousElementSibling.value;

          if( email === '' ) alert('Email diperlukan untuk memulai berlangganan');
          else {
            // _registerEmail(email, function(response) {
            //   alert('Terima kasih. Email anda telah berhasil didaftarkan');
            //   console.log(response);
            // });
            // 
            var apiTarget = 'subscribe/'+ encodeURIComponent(email) +'/1';
            console.log(email, apiTarget);
            var request = APIRequest('GET', apiTarget, {
              onload: function(response) {
                console.log(response);
              }
            });

            request.send(null);
          }
        }
      };
    }());

    app.topbar = (function() {
      var _elem_root = document.getElementById( app.params.topbarContainerId ),
          _elem_togglersContainer = _elem_root.querySelector('#app-controls');

      return {
        container: _elem_root,
        breadcrumb: _elem_root.querySelector('#breadcrumb-label'),
        searchForm: _elem_root.querySelector('#search-field'),
        togglers: _elem_togglersContainer.querySelectorAll( '.'+ app.params.pageTogglerClass ),
        /**
         * [init description]
         * @return {[type]} [description]
         */
        init: function() {
          var context = this;

          _elem_togglersContainer.addEventListener('click', context.onTogglerClick, false);
        },
        /**
         * [getStyle description]
         * @return {[type]} [description]
         */
        getStyle: function() {
          return this.container.className;
        },
        /**
         * [setStyle description]
         * @param {[type]} translucent [description]
         * @param {[type]} shadow      [description]
         * @param {[type]} returnClass [description]
         */
        setStyle: function(translucent, shadow, returnClass) {
          if( typeof returnClass === 'undefined' && typeof returnClass !== 'boolean' ) returnClass = false;

          var newClassName = '';

          if      ( translucent && !shadow )   newClassName = 'translucent no-shadow';
          else if ( !translucent && shadow )   newClassName = '';
          else if ( !translucent && !shadow )  newClassName = 'no-shadow';
          else return;

          if( returnClass ) return newClassName;
          else this.container.className = newClassName;
        },
        /**
         * [setTogglerVisibility description]
         * @param {[type]} buttonIndex [description]
         * @param {[type]} visibility  [description]
         */
        setTogglerVisibility: function(buttonIndex, visibility) {
          if( isArray(buttonIndex) ) {}
          else {
            if( visibility == 'show' ) {
              if( this.togglers[buttonIndex].parentNode.classList.contains( app.params.hiddenClass ) ) {
                this.togglers[buttonIndex].parentNode.classList.remove( app.params.hiddenClass );
              }
            }
            else if( visibility == 'hide' ) {
              if( !( this.togglers[buttonIndex].parentNode.classList.contains( app.params.hiddenClass ) ) ) {
                this.togglers[buttonIndex].parentNode.className += ' ' + app.params.hiddenClass;
              }
            }
          }
        },
        /**
         * [update description]
         * @param  {[type]} toggler    [description]
         * @param  {[type]} pageBefore [description]
         * @return {[type]}            [description]
         */
        update: function(toggler, pageBefore) {
          var breadcrumb = '',
              iconClassNames = [],
              togglerPageTarget = [],
              containerClass = '',
              i, max_i;

          switch( app.activePage ) {
            case 'landing':
              // set breadcrumb text
              this.breadcrumb.textContent = app.params.landingHeader;
              this.breadcrumb.className = 'label-logo';

              // set toggler properties
              if( app.isDesktop ) {
                this.togglers[0].children[0].className = 'iconorima icon-logo';
                this.togglers[0].dataset.pageTarget = '';
              }
              else {
                if( this.togglers[0].parentNode.classList.contains( app.params.hiddenClass ) ) {
                  this.togglers[0].parentNode.classList.remove( app.params.hiddenClass );
                }
                this.togglers[0].children[0].className = 'iconorima icon-menu';
                this.togglers[0].dataset.pageTarget = 'rubrics';
              }
              if( app.landing.article instanceof Article ) {
                this.setTogglerVisibility( 1, 'show' );
              }
              else {
                this.setTogglerVisibility( 1, 'hide' );
              }
              if( this.togglers[2].children[0].classList.contains('icon-close') ) {
                this.togglers[2].children[0].className = 'iconorima icon-search';
                this.togglers[2].dataset.pageTarget = 'search';
              }
              if( this.togglers[2].parentNode.classList.contains( app.params.hiddenClass ) ) {
                this.setTogglerVisibility( 2, 'show' );
              }

              // set topbar container class
              if( app.landing.swiper  instanceof Swiper &&
                  app.landing.article instanceof Article )
              {
                this.setStyle(false, true);
              }
              else {
                this.setStyle(true, false);
              }
            break;

            case 'read':
              /*breadcrumb = app.params.readHeader;
              iconClassNames = [
                'icon-arrow-back',
                'icon-share',
                // (pageBefore !== 'search') ? 'icon-search' : app.params.hiddenClass
                'icon-search'
              ];
              togglerPageTarget = [
                'landing',
                'sharing',
                // (pageBefore !== 'search') ? 'search' : ''
                'search'
              ];
              // containerClass = ( pageBefore !== 'landing' ) ? this.setStyle(false, true, true) : this.setStyle(true, false, true);
              containerClass = this.setStyle(false, true, true);*/

              // set breadcrumb text
              this.breadcrumb.innerText = app.params.readHeader;
              this.breadcrumb.className = '';

              // set toggler properties
              if( this.togglers[0].parentNode.classList.contains( app.params.hiddenClass ) ) {
                this.togglers[0].parentNode.classList.remove( app.params.hiddenClass );
              }
              this.togglers[0].children[0].className = 'iconorima icon-arrow-back';
              this.setTogglerVisibility( 1, 'show' );
              this.setTogglerVisibility( 2, 'hide' );
              // this.togglers[2].children[0].className = 'iconorima icon-search';

              // set topbar container class
              this.setStyle(false, true);
            break;

            case 'rubrics':
              if( !app.isDesktop ) {
                // set breadcrumb text
                this.breadcrumb.textContent = app.params.rubricsHeader;
                if( app.isDesktop ) {
                  this.breadcrumb.className = 'label-logo';
                }
                else {
                  this.breadcrumb.className = '';
                }

                // set toggler properties
                if( !app.isDesktop ) {
                  if( this.togglers[0].parentNode.classList.contains( app.params.hiddenClass ) ) {
                    this.togglers[0].parentNode.classList.remove( app.params.hiddenClass );
                  }
                  this.togglers[0].children[0].className = 'iconorima icon-arrow-back';
                  this.togglers[0].dataset.pageTarget = 'landing';
                }
                this.setTogglerVisibility( 1, 'hide' );
                if( this.togglers[2].parentNode.classList.contains( app.params.hiddenClass ) ) {
                  this.togglers[2].parentNode.classList.remove( app.params.hiddenClass );
                }
                if( this.togglers[2].children[0].classList.contains('icon-close') ) {
                  this.togglers[2].children[0].className = 'iconorima icon-search';
                }

                // set topbar container class
                if( !app.isDesktop ) {
                  this.setStyle(false, true);
                }
              }
            break;

            case 'search':
              // set breadcrumb text
              if( !app.isDesktop ) {
                this.breadcrumb.className = app.params.hiddenClass;
              }

              // set toggler properties
              if( !app.isDesktop ) {
                this.setTogglerVisibility( 0, 'hide' );
              }
              this.setTogglerVisibility( 1, 'hide' );
              this.togglers[2].children[0].className = 'iconorima icon-close dark';
              if( this.togglers[2].parentNode.classList.contains( app.params.hiddenClass ) ) {
                this.setTogglerVisibility( 2, 'show' );
              }
              // this.togglers[2].dataset.pageTarget = pageBefore;

              // set topbar container class
              if( !app.isDesktop ) {
                this.setStyle(false, true);
              }
            break;

            case 'sharing':
              // breadcrumb = app.params.sharingHeader;
              // iconClassNames = [
              //   app.params.hiddenClass,
              //   'icon-close',
              //   app.params.hiddenClass
              // ];
              // togglerPageTarget = [
              //   '',
              //   pageBefore,
              //   ''
              // ];
              // containerClass = this.setStyle(true, false, true);
            break;
          }

          // set search form visibility
          if( app.activePage === 'search' ) {
            this.searchForm.className = '';
          }
          else {
            this.searchForm.className = app.params.hiddenClass;
          }
        },
        /**
         * [onTogglerClick description]
         * @return {[type]} [description]
         */
        onTogglerClick: function() {
          var context = app.topbar,
              toggler = closestParent(event.target, ['a', 'button'], _elem_togglersContainer);

          if( toggler ) {
            event.preventDefault();
            event.stopPropagation();

            var togglerIndex = Array.prototype.indexOf.call( context.togglers, toggler ),
                targetPage = toggler.dataset.pageTarget,
                targetHref = toggler.getAttribute('href'),
                currentPage = app.activePage,
                currentHref = app.url.path,
                isMovingBack = (
                  toggler.children[0].classList.contains('icon-arrow-back') ||
                  toggler.children[0].classList.contains('icon-close')
                );

            if( app.isDesktop && currentPage === 'landing' && togglerIndex === 0 ) return;

            // var destroyCurrent = (
            //   ( pageTarget === 'landing' ) ||
            //   ( pageTarget === 'rubrics' && ( currentView === 'search' || currentView === 'read' ) ) ||
            //   ( pageTarget === 'search' && currentView !== 'read' )
            // );

            // var pageIsMovingBack = (
            //   (
            //     toggler.children[0].classList.contains('icon-arrow-back') ||
            //     toggler.children[0].classList.contains('icon-close')
            //   ) && (
            //     app.rhl.states.length > 1 &&
            //     app.rhl.states[ app.rhl.previousIndex ].previousPage !== null
            //   )
            // );

            // console.log(event.target);

            // if( pageTarget !== 'sharing' ) {
            //   app.activePage = pageTarget;
            // }
            // app.activePage = pageTarget;

            // context.update(toggler, currentView);

            // app[ pageTarget ].setActive(function() {
            //   if( pageIsMovingBack ) { app.rhl.moveBack(); }
            //   else                   { app.rhl.add(); }

            //   if( app.activePage !== 'sharing' ) {
            //     if( app.isDesktop && pageTarget !== 'search' ) {
            //       app[ currentView ].setInactive();
            //     }

            //     if( destroyCurrent && typeof app[currentView].destroy !== 'undefined' ) {
            //       app[currentView].destroy();
            //     }
            //     // if( !app.sharing.toggler.classList.contains( app.params.hiddenClass ) ) {
            //     //   app.sharing.toggler.className = app.params.hiddenClass;
            //     // }
            //     if( !context.togglers[1].classList.contains( app.params.hiddenClass ) &&
            //          ( app.activePage !== 'landing' && app.activePage !== 'read' )
            //       )
            //     {
            //       context.setTogglerVisibility( 1, 'hide' );
            //     }
            //   }
            // });

            if( targetHref !== null ) {
              app.url.path = targetHref;
            }

            if( isMovingBack ) {
              if( app.rhl.states.length > 1 ) {
                app.rhl.moveBack();
              }
              else if( app.rhl.states.length === 1 ) {
                if( toggler.dataset.pageTarget === 'landing' ) {
                  // 
                }
              }
              else return;

              app[ currentPage ].setInactive(function() {
                if( typeof app[ currentPage ].destroy !== 'undefined' ) {
                  app[ currentPage ].destroy();
                }
              });

              app.activePage = targetPage;
            }
            else {
              app.activePage = targetPage;
              app.rhl.add();
            }
            
            console.log( app.rhl.states );
            toggler.dataset.pageTarget = currentPage;
            if( targetHref !== null ) {
              toggler.setAttribute( 'href', currentHref );
            }
            context.update();
            
            app[ app.activePage ].setActive();
          }
        }
      };
    }());
    // console.log(app.topbar);





    // @deprecated
    app.__topbar = {
      container: app.container.querySelector( '#' + app.params.topbarContainerId ),
      togglers: this.container.querySelectorAll( '.' + app.params.pageTogglerClass ),
      breadcrumb: this.container.querySelector('#view-control'),
      searchForm: this.container.querySelector('#search-form'),
      controllers: {},
      /**
       * [setStyle description]
       * @param {[type]} translucent [description]
       * @param {[type]} shadow      [description]
       * @param {[type]} returnClass [description]
       */
      setStyle: function(translucent, shadow, returnClass) {
        if( typeof returnClass === 'undefined' && typeof returnClass !== 'boolean' ) returnClass = false;

        var newClassName = '';

        if      ( translucent && !shadow )   newClassName = 'translucent no-shadow';
        else if ( !translucent && shadow )   newClassName = '';
        else if ( !translucent && !shadow )  newClassName = 'no-shadow';
        else return;

        if( returnClass ) return newClassName;
        else this.container.className = newClassName;
      },
      /**
       * [update description]
       * @param  {[type]} eventTarget [description]
       * @param  {[type]} pageTarget  [description]
       * @return {[type]}             [description]
       */
      update: function(toggler, pageBefore) {
        var breadcrumb = '',
            togglerClasses = [],
            togglerPageTarget = [],
            containerClass = '',
            inputBar = false,
            i = 0, max = this.togglers.length;

        if( app.activePage === 'landing' ) {
          breadcrumb = app.params.landingHeader;
          togglerClasses = [
            ( app.isDesktop ) ? 'icon-logo' : 'icon-menu',
            'icon-search',
            (app.landing.news.activeSubsContent == 'cover') ? app.params.hiddenClass : 'icon-share'
          ];
          togglerPageTarget = [
            ( app.isDesktop ) ? '' : 'rubrics',
            'search',
            'sharing'
          ];
          if( app.landing.swiper instanceof Swiper ) {
            var swiperActiveIndex = app.landing.swiper.activeIndex,
                newsActiveIndex = app.landing.news[ swiperActiveIndex ].activeIndex;
            containerClass = ( newsActiveIndex === 0 ) ? this.setStyle(true, false, true) : this.setStyle(false, true, true);
          }
          else {
            containerClass = this.setStyle(true, false, true);
          }
        }
        else if( app.activePage === 'rubrics' ) {
          breadcrumb = app.params.rubricsHeader;
          togglerClasses = [
            'icon-arrow-back',
            'icon-search',
            app.params.hiddenClass
          ];
          togglerPageTarget = [
            'landing',
            'search',
            'sharing'
          ];
          containerClass = this.setStyle(false, true, true);
        }
        else if( app.activePage === 'search' ) {
          breadcrumb = ( app.isDesktop ) ? 'RIMANEWS' : app.params.searchHeader;
          togglerClasses = [
            ( app.isDesktop ) ? 'icon-logo' : app.params.hiddenClass,
            'icon-close',
            app.params.hiddenClass
          ];
          togglerPageTarget = [
            '',
            (pageBefore !== 'read') ? pageBefore : this.togglers[1].dataset.pageTarget,
            ''
          ];
          containerClass = this.setStyle(false, true, true);
          inputBar = true;
        }
        else if( app.activePage === 'read' ) {
          console.log( pageBefore );
          breadcrumb = app.params.readHeader;
          togglerClasses = [
            'icon-arrow-back',
            (pageBefore !== 'search') ? 'icon-search' : app.params.hiddenClass,
            'icon-share'
          ];
          togglerPageTarget = [
            pageBefore,
            (pageBefore !== 'search') ? 'search' : '',
            'sharing'
          ];
          containerClass = ( pageBefore !== 'landing' ) ? this.setStyle(false, true, true) : this.setStyle(true, false, true);
        }

        this.breadcrumb.textContent = breadcrumb;
        this.container.className = containerClass;

        if( app.activePage === 'search' )
          this.searchForm.className = '';
        else this.searchForm.className = app.params.hiddenClass;

        for( i; i < max; i += 1 ) {
          if( togglerClasses[i] == app.params.hiddenClass )
            this.toggleButtonVisibility( i, 'hide' );
          else {
            if( this.togglers[i].parentNode.classList.contains( app.params.hiddenClass ) )
              this.toggleButtonVisibility( i, 'show' );

            this.togglers[i].firstElementChild.className = 'iconorima ' + togglerClasses[i];
          }

          this.togglers[i].dataset.pageTarget = togglerPageTarget[i];
        }
      },
      /**
       * [toggleButtonVisibility description]
       * @param  {[type]} buttonIndex [description]
       * @param  {[type]} visibility  [description]
       * @return {[type]}             [description]
       */
      toggleButtonVisibility: function(buttonIndex, visibility) {
        if( isArray(buttonIndex) ) {}
        else {
          if( visibility == 'show' ) {
            this.togglers[buttonIndex].parentNode.className += ' ' + app.params.showClass;
            this.togglers[buttonIndex].parentNode.classList.remove( app.params.hiddenClass );
          }
          else if( visibility == 'hide' ) {
            this.togglers[buttonIndex].parentNode.className += ' ' + app.params.hiddenClass;
            this.togglers[buttonIndex].parentNode.classList.remove( app.params.showClass );
          }
        }
      },
      /**
       * [init description]
       * @return {[type]} [description]
       */
      init: function() {
        var _this = this;

        Array.from( _this.togglers ).forEach(function(toggler) {
          toggler.onclick = function(event) {
            app.eventPrevention(event);
            app.eventStopPropagation(event);

            var pageTarget = this.dataset.pageTarget,
                currentView = app.activePage;

            if( app.isDesktop && currentView === 'landing' && pageTarget === '' ) return;

            var destroyCurrent = (
              ( pageTarget === 'landing' ) ||
              ( pageTarget === 'rubrics' && ( currentView === 'search' || currentView === 'read' ) ) ||
              ( pageTarget === 'search' && currentView === 'read' )
            );
            var pageIsMovingBack = (
              (
                toggler.children[0].classList.contains('icon-arrow-back') ||
                toggler.children[0].classList.contains('icon-close')
              ) &&
              ( history.state.previousPage !== null )
            );

            if( pageIsMovingBack ) {
              app.rhl.setNewHistory('back');
            }
            else {
              app.rhl.setNewHistory('push', app.url.path, toggler.getAttribute('href'));
            }

            console.log(event.target);
            app.url.path = toggler.getAttribute('href');
            app.activePage = pageTarget;
            _this.update(toggler, currentView);

            app[ pageTarget ].setActive(function() {
              app[ currentView ].setInactive();
              if( destroyCurrent && typeof app[currentView].destroy !== 'undefined' ) {
                app[currentView].destroy();
              }
              if( !app.sharing.toggler.classList.contains( app.params.hiddenClass ) ) {
                app.sharing.toggler.className = app.params.hiddenClass;
              }
            });
          };
        });
      }
    };

    //////////////////////
    // Init and Destroy //
    //////////////////////
    /**
     * Instance initializer
     * @return {[type]} [description]
     */
    app.init = function() {
      // Init history state object, only if client is
      // accessing the site for the first time.
      app.rhl = new RHL();
      app.rhl.replace( document.title.split(" | ")[1] );
      // Init components
      app.topbar.init();
      app.sharing.init();
      // Init pages
      if( app.activePage === 'landing' ) {
        app.landing.init();
      }
      else if( app.activePage === 'read' ) {
        app.read.init(function() {
          // @deprecated
          // var slide = app.read.news.super.slides[0],
          //     article = slide.querySelector('article > div.news-entry'),
          //     openingParagraph = article.children[1],
          //     rimaFlag = openingParagraph.childNodes[0],
          //     flagGarbage, charCount = 0,
          //     i = 0, max_i = 0;

          // @todo
          // check on vm
          var article = app.read.container.querySelector('article > div.news-entry'),
              openingParagraph = article.children[1],
              rimaFlag = openingParagraph.childNodes[0],
              flagGarbage, charCount = 0,
              i, max_i;

          openingParagraph.removeChild(rimaFlag);
          flagGarbage = openingParagraph.innerHTML.substring(0,3);
          for( i = 0, max_i = flagGarbage.length; i < max_i; i += 1 ) {
            if( flagGarbage.charAt(i) == ' ' || flagGarbage.charAt(i) == '-' ) {
              charCount += 1;
            }
            else if( flagGarbage.charAt(i) == '&' ) {
              charCount += 6;
            }
          }
          openingParagraph.innerHTML = openingParagraph.innerHTML.substring(charCount);

          app.topbar.togglers[1].parentNode.classList.remove( app.params.hiddenClass );
        });
      }
      else if( app.activePage === 'search' ) {
        app.search.init();
      }

      if( !app.isDesktop ) { // users using mobile device
        if( app.activePage === 'rubrics' ) app.rubrics.init();
      }
      else { // users using desktop device
        app.rubrics.init();
      }
    };

    // Initialize plugin
    app.init();
    // Return instance properties
    return app;
  };

  RimanewsWebApp.prototype = {
    get activePage() { return this.container.dataset.activePage; },
    set activePage(componentName) { this.container.dataset.activePage = componentName; }
  };

  // Add instance to window property so we can declare it as an object
  window.RimanewsWebApp = RimanewsWebApp;
})();