<?php
  include "api/db/connect.php";

  $CON = connect();

  define(ROOT, $_SERVER['DOCUMENT_ROOT']);

  // Do mobile detection
  $mobileDetect = new Mobile_Detect;
  $clientDevice = ( $mobileDetect->isMobile() ) ? 'mobile' : 'desktop';
  if( $clientDevice == 'mobile' && $mobileDetect->isTablet() ) $clientDevice .= " tablet";

  if( isset( $_SERVER['REQUEST_URI'] ) ) {
    // Explode path into array first, to check
    // if path has query string or not
    $path = explode( '?', $_SERVER['REQUEST_URI'] );
    $appCurrentComponent = '';

    if( $path[0] == '/' ) {
      // Assign current component variable by first URI segment
      $appCurrentComponent = 'landing';
    }
    else {
      // Get URI segments
      $uriTemp = explode( '/', $path[0] );
      $uris = [];
      for( $i = 0; $i < count($uriTemp); $i += 1 ) {
        if( strlen( $uriTemp[$i] ) > 0 ) $uris[] = $uriTemp[$i];
      }

      // Check page by URI and redirect if condition is true
      if( $uris[0] == 'rubrics' && $clientDevice == 'desktop' ) {
        header( 'Location: /' );
        die();
      }
	  else if( count($uris) > 3 && ( $uris[0] == 'advert' && $uris[1] == 'read' ) ) {
        $uriIdIndex = 3;
        $hasilBerita = array();
        $hasilBerita = AdvertHandler::getBeritaByID( $uris[ $uriIdIndex ], true );

        if( count($hasilBerita) === 0 ) {
          http_response_code(404);
          include ROOT.'/view/error/404.html';
          die();
        }
        
        // Assign current component variable by third URI segment
        $appCurrentComponent = "read";
      }
      else if( count($uris) > 3 && ( $uris[1] == 'read' || $uris[2] == 'read' ) ) {
        $uriIdIndex = ( $uris[2] == 'read' ) ? 4 : 3;
        $hasilBerita = array();
        $hasilBerita = BeritaHandler::getBeritaByID( $uris[ $uriIdIndex ], true );

        if( count($hasilBerita) === 0 ) {
          http_response_code(404);
          include ROOT.'/view/error/404.html';
          die();
        }
        
        // Assign current component variable by third URI segment
        $appCurrentComponent = ( $uris[1] == 'read' ) ? $uris[1] : $uris[2];
      }
      else {
        if( $uris[0] != 'rubrics' && $uris[0] != 'search' ) {
          http_response_code(404);
          include ROOT.'/view/error/404.html';
          die();
        }

        // Assign current component variable by first URI segment
        $appCurrentComponent = $uris[0];
      }
    }

    // Get query string if any
    if( count($path) > 1 && $path[1] != '' ) parse_str( $path[1], $qsas );
  }

  // #app-current-view conditions and states
  if( $appCurrentComponent == 'landing' ) {
    $currentPageLabel = 'RIMANEWS';
    $topbarClass = 'translucent no-shadow';
    $navObjects = (object) array(
      'left' => (object) array(
        'icon' => ($clientDevice === 'mobile' || $clientDevice === 'mobile tablet') ? 'icon-menu' : 'icon-logo',
        'target' => ($clientDevice === 'mobile' || $clientDevice === 'mobile tablet') ? 'rubrics' : '',
        'href' => ($clientDevice === 'mobile' || $clientDevice === 'mobile tablet') ? '/rubrics/' : '/'
      ),
      'right' => (object) array(
        'icon' => 'icon-search',
        'target' => 'search',
        'href' => '/search/'
      )
    );
    $visibilityClass = ['hidden', 'hidden-xs hidden-sm hidden-md', 'hidden'];
  }
  else if( $appCurrentComponent == 'read' ) {
    $currentPageLabel = 'NEWS';
    $topbarClass = '';
    $navObjects = (object) array(
      'left' => (object) array(
        'icon' => 'icon-arrow-back',
        'target' => 'landing',
        'href' => '/'
      ),
      'right' => (object) array(
        'icon' => 'icon-search',
        'target' => 'search',
        'href' => '/search/'
      )
    );
    $visibilityClass = ['show', 'hidden-xs hidden-sm hidden-md', 'hidden'];
  }
  else if( $appCurrentComponent == 'rubrics' ) {
    $currentPageLabel = 'SECTIONS';
    $topbarClass = '';
    $navObjects = (object) array(
      'left' => (object) array(
        'icon' => 'icon-arrow-back',
        'target' => 'landing',
        'href' => '/'
      ),
      'right' => (object) array(
        'icon' => 'icon-search',
        'target' => 'search',
        'href' => '/search/'
      )
    );
    $visibilityClass = ['hidden', 'show', 'hidden'];
  }
  else if( $appCurrentComponent == 'search' ) {
    $currentPageLabel = '';
    $topbarClass = '';
    $navObjects = (object) array(
      'left' => (object) array(
        'icon' => '',
        'target' => '',
        'href' => ''
      ),
      'right' => (object) array(
        'icon' => 'icon-close',
        'target' => 'landing',
        'href' => '/'
      )
    );
    $visibilityClass = ['hidden', 'hidden-xs hidden-sm hidden-md', 'show'];
  }
  $searchFormClass = ( $appCurrentComponent != 'search' ) ? 'hidden' : '';
?>
<!DOCTYPE html>
<html lang="en" class="<?php echo $clientDevice; ?>">
<head>
  <!-- Meta -->
  <meta charset="UTF-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">
  <meta content="id" name="language">
  <meta content="id" name="geo.country">
  <meta name="geo.region" content="ID-JK" />
  <meta name="geo.placename" content="Jakarta" />
  <meta name="geo.position" content="-6.28139;106.794314" />
  <meta name="ICBM" content="-6.28139, 106.794314" />
  <?php
    if( $appCurrentComponent === 'read' ) {
      echo '<meta name="og:image" content="'.$hasilBerita[0]->getImgUrl().'">';
      echo '<meta name="og:description" content="'.$hasilBerita[0]->previewTxt(250).'">';
      echo '<meta name="description" content="'.$hasilBerita[0]->previewTxt(250).'">';
      echo '<title>Rimanews.com | '.$hasilBerita[0]->judul.'</title>';
    }
    else {
      echo '<meta name="description" content="Berita Online Politik Indonesia Terbaru dan Terpercaya | Indonesian Politics Latest News #BersuaraDgnHati">';
      echo '<meta name="keywords" content="rimanews, berita, indonesia, politik, ekonomi, hukum, kriminal, gaya hidup, nasional, internasional, berita indonesia, berita indonesia terkini, berita indonesia terbaru, situs berita indonesia">';
      if( $appCurrentComponent === 'landing' ) {
        echo '<title>Rimanews.com</title>';
      }
      else if( $appCurrentComponent === 'rubrics' ) {
        echo '<title>Rimanews.com | Section</title>';
      }
      else if( $appCurrentComponent === 'search' ) {
        echo '<title>Rimanews.com | Search</title>';
      }
    }
  ?>
  <!-- Android 5.0 WebApp -->
  <link rel="icon" sizes="192x192" href="/public/images/webapp-icon.png">
  <meta name="theme-color" content="#2196F3">
  <!-- iOS WebApp -->
  <link rel="apple-touch-icon" href="/public/images/webapp-icon.png">
  <meta name="apple-mobile-web-app-capable" content="yes">
  <meta name="apple-mobile-web-app-title" content="Rimanews">
  <meta name="format-detection" content="telephone=no">
  <!-- Styles -->
  <link rel="shortcut icon" type="image/x-icon" href="/public/images/favicon.ico">
  <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Lato:300,300i,400,400i,700,700i">
  <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/animate.css/3.5.2/animate.css" />
  <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/Swiper/3.4.0/css/swiper.min.css">
  <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
  <link rel="stylesheet" href="/public/css/rimanews.css">
  <!-- Script -->
  <script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.17.1/moment.min.js"></script>
  <script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.17.1/locale/id.js"></script>
  <script type="text/javascript">
    'use strict';

    // Set browser size as dataset
    var grid = '',
        clientWidth = document.documentElement.clientWidth;
    if      ( clientWidth < 768 )                         { grid = 'xs'; }
    else if ( clientWidth >= 768 && clientWidth < 992 )   { grid = 'sm'; }
    else if ( clientWidth >= 992 && clientWidth < 1199 )  { grid = 'md'; }
    else if ( clientWidth >= 1200 && clientWidth < 1799 ) { grid = 'lg'; }
    else if ( clientWidth >= 1800 )                       { grid = 'xlg'; }
    document.documentElement.dataset.gridType = grid;

    // Set moment.js locale based on <html> lang attribute's value
    moment.locale( document.documentElement.lang );

    /**
     * Check whether local storage is supported or not
     * @return {Boolean}
     */
    function storageIsAvailable() {
      try {
        var storage = window['localStorage'],
            sample = '__storage_test__';

        storage.setItem(sample, sample);
        storage.removeItem(sample);

        return true;
      }
      catch(error) { return false; }
    };

    if( storageIsAvailable() ) {
      if( localStorage.getItem('lastVisit') === null ) {
        localStorage.setItem( 'lastVisit', moment.unix() );
        localStorage.setItem( 'showTutorial', true );
      }
      else {
        var previousVisit = parseInt( localStorage.getItem('lastVisit') ),
            currentVisit = moment.unix();

        localStorage.setItem( 'lastVisit', currentVisit );
      }
    }
    else { /*apply ajax cookies logic here*/ }
  </script>
  <script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/Swiper/3.4.1/js/swiper.min.js"></script>
  <script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/blazy/1.8.2/blazy.min.js"></script>
  <script type="text/javascript" src="/public/js/rimanews.min.js"></script>
</head>
<body>
  <div id="fb-root"></div>
  <script>window.fbAsyncInit=function(){FB.init({appId:'1443477412580329',xfbml:true,version:'v2.8'});FB.AppEvents.logPageView();};(function(d,s,id){var js,fjs=d.getElementsByTagName(s)[0];if(d.getElementById(id)){return;}js=d.createElement(s);js.id=id;js.src="//connect.facebook.net/en_US/sdk.js";fjs.parentNode.insertBefore(js, fjs);}(document,'script','facebook-jssdk'));</script>
  <div id="webapp" class="container-fluid" data-active-page="<?php echo $appCurrentComponent; ?>">
    <!-- App Topbar -->
    <header id="app-master-topbar" class="<?php echo $topbarClass; ?>" role="banner">
      <div id="topbar-wrapper" class="row clearfix">
        <div id="app-breadcrumb">
          <h1 id="breadcrumb-label" <?php if( $appCurrentComponent == 'landing' ) echo 'class="label-logo"'; ?>><?php echo $currentPageLabel; ?></h1>
        </div>
        <div id="app-controls" class="col-xs-12 col-sm-12 col-md-12 col-lg-12 clearfix">
          <div id="ctrl__rubrics" class="control-button">
            <a href="<?php echo $navObjects->left->href; ?>" class="component-toggler js-prevent" role="button" data-page-target="<?php echo $navObjects->left->target; ?>"><i class="iconorima <?php echo $navObjects->left->icon; ?>"></i></a>
          </div>
          <div class="control-group pull-right clearfix">
            <div id="ctrl__share" class="control-button pull-left hidden">
              <button class="component-toggler" role="button" data-page-target="sharing"><i class="iconorima icon-share"></i></button>
            </div>
            <div id="ctrl__search" class="control-button pull-left">
              <a href="<?php echo $navObjects->right->href; ?>" class="component-toggler js-prevent" role="button" data-page-target="<?php echo $navObjects->right->target ?>"><i class="iconorima <?php echo $navObjects->right->icon; ?>"></i></a>
            </div>
          </div>
        </div>
        <div id="search-field" class="<?php echo $searchFormClass; ?>">
          <form id="search-form" action="#">
            <a href="/search/" id="cse-back-trigger" class="js-prevent hidden" role="button"><i class="iconorima icon-arrow-back"></i></a>
            <input type="search" name="search" placeholder="Type keywords or topics..." />
          </form>
        </div>
      </div>
    </header>
    <!-- App Content -->
    <main id="app-content" class="row" role="main">

      <div id="read" class="col-xs-12 col-sm-12 col-lg-9 col-lg-offset-3 component-wrapper no-col-side-padding <?php echo $visibilityClass[0]; ?>">
        <?php
          if( $appCurrentComponent === 'read') {
            //$hasilBerita=BeritaHandler::getBeritaByID($_GET['hash'],true);

            $dataBerita=$hasilBerita[0];
            $safeTitle=str_replace("\"","\\\"",$dataBerita->judul);
            $titleFontClass = 'font-';
            if( strlen($dataBerita->judul) < 30 )
              $titleFontClass .= 'xlarge';
            else if( strlen($dataBerita->judul) >= 30 && strlen($dataBerita->judul) <= 50 )
              $titleFontClass .= 'large';
            else // title length > 50
              $titleFontClass .= 'normal';

            $newsURL = $dataBerita->getBeritaUrlDesktop();
            $newsURI = explode('rimanews.com', $newsURL)[1];
            $dateStr = $dataBerita->getPublishDate3();
            $dateArr = explode(' ', $dateStr);

            switch($dateArr[1]) {
              case 'Agustus':
                $markupDate = $dateArr[0].' Ag<span class="long-date-tail">u</span>s<span class="long-date-tail">tus</span> '.$dateArr[2];
                break;
              case 'Mei':
                $markupDate = $dateStr;
                break;
              default:
                $markupDate = $dateArr[0].' '.substr($dateArr[1], 0, 3).'<span class="long-date-tail">'.substr($dateArr[1], 3).'</span> '.$dateArr[2];
                break;
            }

            echo '
              <article class="clearfix">
                <header class="col-xs-12 col-sm-12 col-lg-10 col-lg-offset-1 news-header">
                  <div class="news-rubrics">
                    <div class="rubrics-wrapper clearfix">
                      <a href="#!" class="pull-left">'.$dataBerita->parentKategoriUrl.'</a>
                      <span class="pull-left bullet">&#8226;</span>
                      <a href="#!" class="pull-left">'.$dataBerita->kategoriUrl.'</a>
                    </div>
                  </div>
                  <h2 class="news-title">'.$dataBerita->judul.'</h2>
                  <div class="news-meta clearfix">
                    <div class="pull-left meta-entry post-date">
                      <span class="meta-label">PUBLISHED ON:</span>
                      <time class="meta-value" datetime="'.substr($dataBerita->publish,0,10).'T'.substr($dataBerita->publish,11,8).'+07:00",'.'">'.$markupDate.'</time>
                    </div>
                    <div class="pull-left meta-entry reporter">
                      <span class="meta-label">REPORTED BY:</span>
                      <span class="meta-value">'.$dataBerita->createdBy.'</span>
                    </div>
                  </div>
                </header>
                <div class="col-xs-12 col-lg-10 col-lg-offset-1 news-entry">
                  <p class="row">
                    <span class="figure figure-headline">
                      <img class="b-lazy"
                           src="data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAABAAAAAJCAQAAACRI2S5AAAAEElEQVR42mNkIAAYRxWAAQAG9gAKqv6+AwAAAABJRU5ErkJggg=="
                           data-src="'.$dataBerita->getImgUrl().'"
                           alt="'.$safeTitle.'"
                      />
                      <span class="rima-lazy-spinner large block">
                        <span class="spinner clearfix">
                          <span class="bounce1 pull-left"></span>
                          <span class="bounce2 pull-left"></span>
                          <span class="bounce3 pull-left"></span>
                        </span>
                      </span>
                    </span>
                    <span class="caption caption-headline">'.$dataBerita->keteranganGambar.'"</span>
                  </p>
                  '.$dataBerita->isi.'
                </div>
                <footer class="news-footer col-xs-12 col-sm-12 col-md-12 col-lg-12">
                  <div class="row">
                    <p class="subscribe-intro col-xs-8 col-xs-offset-2">
                      <span>
                        <span>Stay up to date with&nbsp;</span><i class="logo-text">Rimanews</i>
                      </span>
                      <span>Get the latest news alerts delivered to your inbox</span>
                    </p>
                  </div>
                  <div class="row">
                    <div class="subscribe-form-wrapper clearfix">
                      <input type="email" name="subscriber_email" placeholder="Your email address">
                      <button class="subscribe-button guest-mode" type="button">Subscribe Now</button>
                    </div>
                  </div>
                  <p class="subscribe-outro">Don\'t worry we won\'t spam you</p>
                </footer>
              </article>
              <!--<div class="fb-comments" data-href="'.$newsURL.'" data-numposts="5" data-width="100%" data-colorscheme="light"></div>-->
            ';
          }
        ?>
      </div>

      <div id="landing" class="col-xs-12 col-sm-12 col-lg-9 col-lg-offset-3 news-container component-wrapper no-col-side-padding swiper-container">
        <?php if( $clientDevice === 'desktop' ): ?>
          <div class="swiper-navigator">
            <button id="landing-prev" type="button"><i class="iconorima icon-chevron-left"></i></button>
            <button id="landing-next" type="button"><i class="iconorima icon-chevron-right"></i></button>
          </div>
        <?php elseif( $clientDevice === 'mobile' || $clientDevice === 'mobile tablet' ): ?>
          <div class="col-xs-12 col-sm-12 news-slide-hint">
            <div class="inner-hint-wrapper clearfix">
              <span class="hint-text prefix pull-left">Swipe Up</span>
              <i class="hint-gesture"></i>
              <span class="hint-text suffix pull-right">To Read</span>
            </div>
          </div>
        <?php endif; ?>
        <div id="article-jumper" class="hidden">
          <button type="button">
            <i class="iconorima icon-arrow-upward"></i>
          </button>
        </div>
        <div class="swiper-wrapper">
          <?php
           $data=BeritaHandler::getBeritaTerbaru(0,0,5,true,false,false,false,false,null,true);
            
            for($i=0;$i<count($data);$i++)
            {
              $safeTitle=str_replace("\"","\\\"",$data[$i]->judul);
              $titleFontClass = 'font-';
              if( strlen($data[$i]->judul) < 30 )
                $titleFontClass .= 'xlarge';
              else if( strlen($data[$i]->judul) >= 30 && strlen($data[$i]->judul) <= 50 )
                $titleFontClass .= 'large';
              else // title length > 50
                $titleFontClass .= 'normal';

              $newsURL = $data[$i]->getBeritaUrlDesktop();
              $newsURI = explode('rimanews.com', $newsURL)[1];

              $dateStr = $data[$i]->getPublishDate3();
              $dateArr = explode(' ', $dateStr);
              switch($dateArr[1]) {
                case 'Agustus':
                  $markupDate = $dateArr[0].' Ag<span class="long-date-tail">u</span>s<span class="long-date-tail">tus</span> '.$dateArr[2];
                  break;
                case 'Mei':
                  $markupDate = $dateStr;
                  break;
                default:
                  $markupDate = $dateArr[0].' '.substr($dateArr[1], 0, 3).'<span class="long-date-tail">'.substr($dateArr[1], 3).'</span> '.$dateArr[2];
                  break;
              }

              echo '
                <div class="swiper-slide" data-hash="'.$data[$i]->id.'" data-title="'.$safeTitle.'" data-bg-type="image" data-uri="'.$newsURI.'">
                  <div id="'.$data[$i]->id.'__pre" class="swiper-container news-content article__pre">
                    <div class="swiper-wrapper news-item-wrapper">
                      <div class="swiper-slide news-item-cover">
                        <div id="'.$data[$i]->id.'-bg-canvas" class="background-wrapper">
                          <img id="'.$data[$i]->id.'-bg-image" class="b-lazy"
                               src="data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAABAAAAAJCAQAAACRI2S5AAAAEElEQVR42mNkIAAYRxWAAQAG9gAKqv6+AwAAAABJRU5ErkJggg=="
                               data-src="'.$data[$i]->getImgUrl().'"
                               alt="'.$safeTitle.'"
                          />
                          <span class="rima-lazy-spinner large block">
                            <span class="spinner clearfix">
                              <span class="bounce1 pull-left"></span>
                              <span class="bounce2 pull-left"></span>
                              <span class="bounce3 pull-left"></span>
                            </span>
                          </span>
                        </div>
                        <div id="'.$data[$i]->id.'-title" class="cover-title bottom '.$titleFontClass.'">';
              if( $clientDevice === 'desktop' ) {
                echo      '<button id="'.$data[$i]->id.'-read-button" class="read-this-button" type="button">Continue reading ...</button>';
              }
              echo     '</div>
                      </div>
                      <div class="swiper-slide news-item-article">
                        <article class="clearfix"></article>
                      </div>
                    </div>
                  </div>
                </div>
              ';
            }
          ?>
          <!-- Preloader Slide -->
          <div class="swiper-slide">
            <div class="swiper-container preloader-content">
              <div class="swiper-wrapper">
                <div class="loader">
                  <i class="spinner"></i>
                  <span class="status">Fetching news ...</span>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>

      <div id="rubrics" class="col-xs-12 col-sm-12 col-lg-3 component-wrapper no-col-side-padding <?php echo $visibilityClass[1]; ?>">
        <nav id="rubrics-nav" class="col-xs-12 col-sm-3 col-lg-3">
          <?php if( $appCurrentComponent === 'rubrics' || $clientDevice === 'desktop' ): ?>
          <ul>
            <li>
              <a class="js-prevent" href="/rubrics/" data-rubrics-id="0">
                <img class="b-lazy" src="data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAABAAAAAJCAQAAACRI2S5AAAAEElEQVR42mNkIAAYRxWAAQAG9gAKqv6+AwAAAABJRU5ErkJggg==" data-src="/public/images/rubrics/todays.png" srcset="/public/images/rubrics/todays@3.5x.png 2x, /public/images/rubrics/todays@3.5x.png 3x" />
                <span class="rima-lazy-spinner small block">
                  <span class="spinner clearfix">
                    <span class="bounce1 pull-left"></span>
                    <span class="bounce2 pull-left"></span>
                    <span class="bounce3 pull-left"></span>
                  </span>
                </span>
                <span class="overlay-accent blue">
                  <span class="overlay-label">Today's</span>
                </span>
              </a>
            </li>
            <li>
              <a class="js-prevent" href="/rubrics/opinion/" data-rubrics-id="11">
                <img class="b-lazy" src="data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAABAAAAAJCAQAAACRI2S5AAAAEElEQVR42mNkIAAYRxWAAQAG9gAKqv6+AwAAAABJRU5ErkJggg==" data-src="/public/images/rubrics/politics3.png" srcset="/public/images/rubrics/politics3@3.5x.png 2x, /public/images/rubrics/politics3@3.5x.png 3x" />
                <span class="rima-lazy-spinner small block">
                  <span class="spinner clearfix">
                    <span class="bounce1 pull-left"></span>
                    <span class="bounce2 pull-left"></span>
                    <span class="bounce3 pull-left"></span>
                  </span>
                </span>
                <span class="overlay-accent red">
                  <span class="overlay-label">Opinion</span>
                </span>
              </a>
            </li>
            <li>
              <a class="js-prevent" href="/rubrics/ideas/" data-rubrics-id="12">
                <img class="b-lazy" src="data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAABAAAAAJCAQAAACRI2S5AAAAEElEQVR42mNkIAAYRxWAAQAG9gAKqv6+AwAAAABJRU5ErkJggg==" data-src="/public/images/rubrics/ideas3.png" srcset="/public/images/rubrics/ideas3@3.5x.png 2x, /public/images/rubrics/ideas3@3.5x.png 3x" />
                <span class="rima-lazy-spinner small block">
                  <span class="spinner clearfix">
                    <span class="bounce1 pull-left"></span>
                    <span class="bounce2 pull-left"></span>
                    <span class="bounce3 pull-left"></span>
                  </span>
                </span>
                <span class="overlay-accent light-blue">
                  <span class="overlay-label">Ideas</span>
                </span>
              </a>
            </li>
            <li>
              <a class="js-prevent" href="/rubrics/readup/" data-rubrics-id="13">
                <img class="b-lazy" src="data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAABAAAAAJCAQAAACRI2S5AAAAEElEQVR42mNkIAAYRxWAAQAG9gAKqv6+AwAAAABJRU5ErkJggg==" data-src="/public/images/rubrics/living2.png" srcset="/public/images/rubrics/living2@3.5x.png 2x, /public/images/rubrics/living2@3.5x.png 3x" />
                <span class="rima-lazy-spinner small block">
                  <span class="spinner clearfix">
                    <span class="bounce1 pull-left"></span>
                    <span class="bounce2 pull-left"></span>
                    <span class="bounce3 pull-left"></span>
                  </span>
                </span>
                <span class="overlay-accent yellow">
                  <span class="overlay-label">Read Up</span>
                </span>
              </a>
            </li>
            
          </ul>
          <?php endif; ?>
        </nav>
        <div id="rubrics-listing" class="col-sm-9 col-lg-9 col-sm-offset-3 col-lg-offset-3 no-col-side-padding">
          <div id="listing-toggler" class="col-xs-12 col-sm-12 col-md-12 col-lg-12 no-col-side-padding clearfix">
            <!-- <span class="pull-left divider">&#8226;</span> -->
            <button class="pull-left active" data-target-index="0" role="button">Newest</button>
            <button class="pull-left" data-target-index="1" role="button">Popular</button>
          </div>
          <div id="listing-content" class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
            <?php if( $appCurrentComponent === 'rubrics' || $clientDevice === 'desktop' ): ?>
            <ul class="category-section show">
              <?php
                $kategoriRubrik=0;
                if($_GET['rub1'] == "opinion")$kategoriRubrik=11;
                else if($_GET['rub1'] == "ideas")$kategoriRubrik=12;
                else if($_GET['rub1'] == "readup")$kategoriRubrik=13;


                $dataBerita=BeritaHandler::getBeritaTerbaru($kategoriRubrik,0,10,true,false,false,false,false,null,true);

                for($i=0;$i<count($dataBerita);$i++)
                {
                  $safeTitle=str_replace("\"","\\\"",$dataBerita[$i]->judul);
                  $servedOnURL = ( $_SERVER['SERVER_NAME'] != 'rimanews.com' ) ?
                                explode( "rimanews.com", $dataBerita[$i]->getBeritaUrlDesktop() )[1] :
                                $dataBerita[$i]->getBeritaUrlDesktop();

                  echo '
                    <li class="regular clearfix">
                      <a href="'.$servedOnURL.'" data-hash="'.$dataBerita[$i]->id.'" data-title="'.$dataBerita[$i]->judul.'">
                        <div class="left-hand pull-left">
                          <div class="image-box">
                            <img class="b-lazy" src="data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAABAAAAAJCAQAAACRI2S5AAAAEElEQVR42mNkIAAYRxWAAQAG9gAKqv6+AwAAAABJRU5ErkJggg==" data-src="'.$dataBerita[$i]->getImgUrl().'" />
                            <span class="rima-lazy-spinner small block">
                              <span class="spinner clearfix">
                                <span class="bounce1 pull-left"></span>
                                <span class="bounce2 pull-left"></span>
                                <span class="bounce3 pull-left"></span>
                              </span>
                            </span>
                          </div>
                          <span class="category-label">'.$dataBerita[$i]->parentKategoriUrl.'</span>
                        </div>
                        <div class="right-hand pull-left">
                          <h2>'.$dataBerita[$i]->judul.'</h2>
                          <time datetime="'.substr($dataBerita[$i]->publish,0,10).'T'.substr($dataBerita[$i]->publish,11,8).'+07:00",'.'">'.date("F j, Y", strtotime($dataBerita[$i]->publish)).'</time>
                        </div>
                      </a>
                    </li>
                  ';
                }
              ?>
            </ul>
            <ul class="category-section hidden">
              <?php
               $dataBerita=BeritaHandler::getBeritaTerpopulerCustom($kategoriRubrik,0,10,false,false,null,true);
        
                for($i=0;$i<count($dataBerita);$i++)
                {
                  $safeTitle=str_replace("\"","\\\"",$dataBerita[$i]->judul);
                  $servedOnURL = ( $_SERVER['SERVER_NAME'] != 'rimanews.com' ) ?
                                explode( "rimanews.com", $dataBerita[$i]->getBeritaUrlDesktop() )[1] :
                                $dataBerita[$i]->getBeritaUrlDesktop();
                
                  echo '
                    <li class="regular clearfix">
                      <a href="'.$servedOnURL.'" data-hash="'.$dataBerita[$i]->id.' "data-title="'.$dataBerita[$i]->judul.'">
                        <div class="left-hand pull-left">
                          <div class="image-box">
                            <img class="b-lazy" src="data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAABAAAAAJCAQAAACRI2S5AAAAEElEQVR42mNkIAAYRxWAAQAG9gAKqv6+AwAAAABJRU5ErkJggg==" data-src="'.$dataBerita[$i]->getImgUrl().'" />
                            <span class="rima-lazy-spinner small block">
                              <span class="spinner clearfix">
                                <span class="bounce1 pull-left"></span>
                                <span class="bounce2 pull-left"></span>
                                <span class="bounce3 pull-left"></span>
                              </span>
                            </span>
                          </div>
                          <span class="category-label">'.$dataBerita[$i]->parentKategoriUrl.'</span>
                        </div>
                        <div class="right-hand pull-left">
                          <h2>'.$dataBerita[$i]->judul.'</h2>
                          <time datetime="'.substr($dataBerita[$i]->publish,0,10).'T'.substr($dataBerita[$i]->publish,11,8).'+07:00",'.'">'.date("F j, Y", strtotime($dataBerita[$i]->publish)).'</time>
                        </div>
                      </a>
                    </li>
                  ';
                }
              ?>
            </ul>
            <?php endif; ?>
            <div id="loadmore">
              <i class="spinner"></i>
              <button>Load More</button>
            </div>
          </div>
        </div>
      </div>

      <div id="search" class="col-xs-12 col-sm-12 col-lg-3 component-wrapper no-col-side-padding animated <?php echo $visibilityClass[2]; ?>">
        <div id="topic-heading" class="col-xs-12 clearfix">
          <button id="topic-back-trigger" class="pull-left hidden" type="button"><i class="iconorima icon-arrow-back"></i></button>
          <h2 id="topic-title" class="pull-left">Most Popular Topics</h2>
        </div>
        <div id="topic-picker" class="col-xs-12 col-sm-12">
          <ul id="topic-listing">
            <?php
              if( $appCurrentComponent === 'search' ) {
                $data=BeritaHandler::getTopikTerpopuler(0,0,10);
                
                for($i=0;$i<count($data);$i++)
                {
                  echo '
                    <li class="animated bounceIn">
                      <div class="tag-wrapper">
                        <a class="js-prevent clearfix" href="/search/tag/'.$data[$i]['kata_kunci_url'].'" data-tag-uri="'.$data[$i]['kata_kunci_url'].'">
                          <h3>'.$data[$i]['kata_kunci'].'</h3>
                          <p>Has been shown '.$data[$i]['jml'].' times</p>
                        </a>
                      </div>
                    </li>
                  ';
                }
              }
            ?>
          </ul>
          <div id="topic-overlay" class="hidden"></div>
        </div>
        <div id="topic-news-picker" class="col-xs-11 col-xs-offset-1 col-sm-10 col-sm-offset-2 animated hidden">
          <ul id="news-topic-listing"></ul>
        </div>
        <div id="search-result" class="col-xs-12 hidden">
          <div class="result-info clearfix hidden">
            <div class="pull-left">Search results: <span id="result-count"></span></div>
            <div class="pull-right">Powered by <span id="result-sponsor">Google</span></div>
          </div>
          <ul id="suggestion-listing" class="hidden"></ul>
          <div id="loadmore-cse" class="hidden">
            <i class="spinner"></i>
            <button>Load More</button>
          </div>
        </div>
      </div>

    </main>
    <!-- Subscribe Modal Content -->
    <div id="subscribe-modal" class="hidden"></div>
    <!-- Sharing Content -->
    <div id="sharing" class="col-xs-12 col-sm-12 col-md-12 col-lg-12 hidden">
      <div id="sharing-overlay"></div>
      <div id="sharing-container" class="row">
        <div class="share-summary">
          <!-- Image -->
        </div>
        <div class="intents-row col-xs-12 col-sm-12 col-md-12 col-lg-8 col-lg-offset-4">
          <div class="facebook col-xs-4 col-sm-4 col-md-4 col-lg-2">
            <button type="button" onclick="Rimanews.sharing.shareTo('fb', this)"><i class="iconorima icon-facebook"></i>Facebook</button>
          </div>
          <div class="twitter col-xs-4 col-sm-4 col-md-4 col-lg-2">
            <a href="#" target="_blank"></a>
            <button type="button" onclick="Rimanews.sharing.shareTo('twitter', this)"><i class="iconorima icon-twitter"></i>Twitter</button>
          </div>
          <div class="gplus col-xs-4 col-sm-4 col-md-4 col-lg-2">
            <a href="#" target="_blank"></a>
            <button type="button" onclick="Rimanews.sharing.shareTo('gplus', this)"><i class="iconorima icon-gplus"></i>Google+</button>
          </div>
          <div class="line col-xs-4 col-sm-4 col-md-4 col-lg-2">
            <a href="#" target="_blank"></a>
            <button type="button" onclick="Rimanews.sharing.shareTo('line', this)"><i class="iconorima icon-line"></i>Line</button>
          </div>
          <div class="whatsapp col-xs-4 col-sm-4 col-md-4 col-lg-2">
            <a href="#" target="_blank"></a>
            <button type="button" onclick="Rimanews.sharing.shareTo('whatsapp', this)"><i class="iconorima icon-whatsapp"></i>Whatsapp</button>
          </div>
        </div>
      </div>
    </div>
    <!-- Overlay -->
    <div id="app-desktop-overlay" class="<?php if( $clientDevice == 'desktop' && $appCurrentComponent == 'search' ) echo "show"; else echo "hidden"; ?>"></div>
  </div>
  <script type="text/javascript">
    var blazy = new Blazy({
      offset: 0,
      success: function(element) {
        if( element.nextElementSibling && element.nextElementSibling.classList.contains('rima-lazy-spinner') ) {
          element.nextElementSibling.remove();
        }
      }
    });
    var Rimanews = new RimanewsWebApp('#webapp', {
      rubricsHeader: 'SECTIONS',
      readHeader: 'NEWS',
      sharingHeader: 'SHARE TO',
    });
  </script>
</body>
</html>

<?php
  mysqli_query($CON);
?>
